# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import
import os
import sys
import urlgrabber.grabber
import urlgrabber.progress
from libtaskotron.logger import log
from libtaskotron.exceptions import TaskotronRemoteError


def makedirs(fullpath):
    '''This is the same as :meth:`os.makedirs`, but does not raise an exception
    when the destination directory already exists.

    :raise OSError: if directory doesn't exist and can't be created
    '''
    try:
        os.makedirs(fullpath)
        assert os.path.isdir(fullpath)
    except OSError, e:
        if e.errno == 17: # "[Errno 17] File exists"
            # if it is a directory everything is ok
            if os.path.isdir(fullpath):
                return
            # otherwise it is a file/socket/etc and it is an error
            else:
                raise
        else:
            raise

def get_urlgrabber(**kwargs):
    '''Get a new instance of :class:`URLGrabber` that is reasonably configured
       (progress bar when in terminal, automatic retries, connection timeout,
       etc)

       :param kwargs: additional kwargs to add to the URLGrabber constructor
       :return: :class:`urlgrabber.grabber.URLGrabber` instance
    '''
    grabber_args = {'retry': 3, 'timeout': 120}
    grabber_args.update(kwargs)

    grabber = urlgrabber.grabber.URLGrabber(**grabber_args)

    # progress bar when in terminal
    if hasattr(sys.stdout, "fileno") and os.isatty(sys.stdout.fileno()):
        grabber.opts.progress_obj = urlgrabber.progress.TextMeter()

    # retry on socket timeout
    if 12 not in grabber.opts.retrycodes:
        grabber.opts.retrycodes.append(12)

    return grabber


def download(url, dirname, filename=None, overwrite=False, grabber=None):
    '''Download a file.

    :param str url: file URL to download
    :param str dirname:  directory path, if the directory does not exist, it gets
                         created (and all its parent directories).
    :param str filename: name of downloaded file, if not provided basename is
                         extracted from URL
    :param bool overwrite: if the destination file already exists, whether to
                           overwrite or not. If ``False``, a simple check is
                           performed whether the remote file is the same as the
                           local file and re-downloads it anyway if they are not
                           the same.
    :param grabber: custom :class:`urlgrabber.grabber.URLGrabber` instance to be
                    used for actual downloading
    :return: file path of the downloaded file
    :rtype: str
    :raise TaskotronRemoteError: if download fails or directory can't be created
    '''
    if not grabber:
        grabber = get_urlgrabber()

    if not filename:
        filename = os.path.basename(url)

    dest = os.path.join(dirname, filename)

    if os.path.exists(dirname):
        if not os.path.isdir(dirname):
            raise TaskotronRemoteError(
                "Can't create directory: %r It is an already "
                "existing file.", dirname)
    else:
        try:
            makedirs(dirname)
        except OSError, e:
            log.exception("Can't create directory: %s", dirname)
            raise TaskotronRemoteError(e)

    # overwrite?
    if os.path.exists(dest):
        if overwrite:
            os.remove(dest)
        else:
            log.debug('Already downloaded: %s', os.path.basename(dest))
            return dest

    log.debug('Downloading: %s', url)
    try:
        grabber.urlgrab(url, dest)
    except urlgrabber.grabber.URLGrabError, e:
        log.exception('Download failed for: %s', url)
        # the file can be incomplete, remove
        if os.path.exists(dest):
            try:
                os.remove(dest)
            except OSError, e:
                log.exception('Could not delete incomplete file: %s', dest)
        raise TaskotronRemoteError(e)

    return dest