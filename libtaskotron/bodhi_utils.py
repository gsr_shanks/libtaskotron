# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Utility functions for dealing with Bodhi'''

from __future__ import absolute_import
import fedora.client

from .logger import log
from . import rpm_utils
from . import python_utils
from . import exceptions as exc
from . import config

class BodhiUtils(object):
    '''Helper Bodhi methods.

    :ivar fedora.client.BodhiClient client: Bodhi client instance
    '''

    def __init__(self, client=None):
        '''Create a new BodhiUtils instance.

        :param client: custom :class:`BodhiClient` instance. If ``None``, a
                       default BodhiClient instance is used.
        '''

        self.taskotron_config = config.get_config()
        username = self.taskotron_config.fas_username
        password = self.taskotron_config.fas_password
        bodhi_url = self.taskotron_config.bodhi_server

        if not client:
            log.debug('creating bodhi client to %s' % bodhi_url)
            self.client = fedora.client.bodhi.BodhiClient(username=username,
                                                          password=password,
                                                          base_url=bodhi_url)
        else:
            self.client = client


    def query_update(self, package):
        '''Get the last Bodhi update matching criteria. Retry
        ``bodhi_request_max_retries``-times when server returns HTTP 5xx response.

           :param str package: package NVR or package name or update title or
                               update ID

                               .. note::
                                Only NVR allowed, not NEVR. See
                                https://fedorahosted.org/bodhi/ticket/592.
           :return: most recent Bodhi update object matching criteria, or
                    ``None`` when no update is found.
           :rtype: :class:`bunch.Bunch`
        '''
        log.debug('Searching Bodhi updates for: %s', package)
        max_retries = self.taskotron_config.bodhi_request_max_retries
        for retry_num in xrange(1, max_retries + 1):
            try:
                res = self.client.query(package=package, limit=1)
                if res['updates']:
                    return res['updates'][0]
                else:
                    return None
            except fedora.client.ServerError as e:
                if retry_num >= max_retries:
                    log.critical('Maximum Bodhi server retries exceeded')
                    raise
                elif e.code < 500 or e.code > 599:
                    raise
                else:
                    log.warning('Got HTTP %d response from bodhi, retrying \
(%d retries remaining)...', e.code, max_retries - retry_num)


    def build2update(self, builds, strict=False):
        '''Find matching Bodhi updates for provided builds.

        :param builds: builds to search for in N(E)VR format (``foo-1.2-3.fc20``
                       or ``foo-4:1.2-3.fc20``)
        :type builds: iterable of str
        :param bool strict: if ``False``, incomplete Bodhi updates are allowed.
                            If ``True``, every Bodhi update will be compared
                            with the set of provided builds. If there is an
                            Bodhi update which contains builds not provided in
                            ``builds``, that update is marked as incomplete and
                            removed from the result - i.e. all builds from
                            ``builds`` that were part of this incomplete update
                            are placed in the second dictionary of the result
                            tuple.
        :return: a tuple of two dictionaries:

                 * The first dict provides mapping between ``builds`` and their
                   updates where no error occured.

                   ``{build (string): Bodhi update (Bunch)}``
                 * The second dict provides mapping between ``builds`` and their
                   updates where some error occured. The value is ``None`` if
                   the matching Bodhi update could not be found (the only
                   possible cause of failure if ``strict=False``). Or the value
                   is a Bodhi update that was incomplete (happens only if
                   ``strict=True``).

                   ``{build (string): Bodhi update (Bunch) or None}``
        :raise TaskotronValueError: if ``builds`` type is incorrect
        '''
        # validate input params
        if not python_utils.iterable(builds, basestring):
            raise exc.TaskotronValueError("Param 'builds' must be an iterable "
                "of strings, and yours was: %s" % type(builds))

        build2update = {}
        failures = {}
        # Bodhi works with NVR only, but we have to ensure we receive and return
        # even NEVR format. So we need to convert internally.
        builds_nvr = set([rpm_utils.rpmformat(build, 'nvr')
                          for build in builds])

        for build in sorted(builds):
            # if already found answer for this build, skip it
            if build in build2update or build in failures:
                continue

            # get Bodhi update object
            update = self.query_update(rpm_utils.rpmformat(build, 'nvr'))
            if update is None:
                # no such Bodhi update
                failures[build] = None
                continue

            # all builds listed in the update
            bodhi_builds = set([build['nvr'] for build in update['builds']])
            # builds *not* provided in @param builds but part of the update
            # (NVRs)
            missing_builds = bodhi_builds.difference(builds_nvr)
            # builds provided in @param builds and part of the update
            matched_builds = [build for build in builds if
                              rpm_utils.rpmformat(build, 'nvr') in bodhi_builds]

            # reject incomplete updates
            if missing_builds and strict:
                for matched_build in matched_builds:
                    failures[matched_build] = update
                continue

            # create item in build2update
            for build in matched_builds:
                build2update[build] = update

        # some builds might have been added to both @var failures and
        # @var build2update. That's probably a Bodhi fault, because a build
        # should not be part of two different updates. If that happens, remove
        # it from @var build2update and keep it in @var failures.
        for build in failures:
            if build in build2update:
                log.warn("The build %s is a part of two different updates: '%s'"
                         " and '%s'. That's probably a bug in Bodhi." % (build,
                         failures[build]['title'], build2update[build]['title']))
                del build2update[build]

        return (build2update, failures)
