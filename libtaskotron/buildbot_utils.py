# -*- coding: utf-8 -*-
# Copyright 2010-2014, Red Hat, Inc.
# License: GNU General Public License version 2 or later

'''Utility functions for dealing with Buildbot'''

from __future__ import absolute_import
from libtaskotron.logger import log


def parse_jobid(jobid):
    """ Parse the incoming jobid which should either be '-1' to indicate
    that dummy values should be used or in the form of <builder>/<buildid>.

    If the passed in format is invalid, no exceptions will be raised but a
    warning message will be emitted to logs

    :param str jobid: jobid from runner
    :returns: (builder, buildid)
    """
    if jobid != "-1":
        try:
            builder, buildid = jobid.split('/')
            return (builder, buildid)
        except ValueError, e:
            log.debug(e)
            log.warning(
                'Invalid jobid format detected, resetting to default '
                'value. jobid must be in builder/buildid format, '
                'found %s' % jobid)

    return "default", "-1"


def get_urls(jobid, masterurl, task_stepname):
    """ Form url to the job and its logs based on given jobid.

    :param str jobid: jobid from runner
    :param str masterurl: taskotron master url
    :param str task_stepname: taskotron master step name
    :returns: (job_url, log_url)
    """
    builder, buildid = parse_jobid(jobid)

    job_url = '%s/builders/%s/builds/%s' % (masterurl, builder, buildid)
    log_url = '%s/steps/%s/logs/stdio' % (job_url, task_stepname)

    return job_url, log_url
