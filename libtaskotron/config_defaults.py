# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''This includes the default values for Taskotron configuration. This is
automatically loaded by config.py and then overridden by values from config
files available in system-wide location.'''

from __future__ import absolute_import
import pprint


class ProfileName(object):
    '''Enum of available profile names. These can be specified in the config
    file or as the environment variable.'''

    DEVELOPMENT = 'development'  #:
    PRODUCTION = 'production'    #:
    TESTING = 'testing'          #:


class Config(object):
    '''Global configuration for Taskotron (development profile).

       The documentation for individual options is available in the config
       files (unless they're not present in the config files, then they're
       documented here).

       Implementation notes:

       * If you want to add a new option, put it here and optionally into the
         config file as well.
       * If you modify a default value for some option, don't forget to modify
         it in both places - here and in the config file (if present).
       * Don't assign ``None`` as a default value. We need to know a value type
         in order to check for correct type of user-provided values.
    '''

    profile = ProfileName.DEVELOPMENT                                       #:

    reporting_enabled = False                                               #:
    report_to_bodhi = True                                                  #:
    report_to_resultsdb = True                                              #:

    koji_url = 'http://koji.fedoraproject.org/kojihub'                      #:
    pkg_url = 'http://kojipkgs.fedoraproject.org/packages'                  #:
    bodhi_server = 'https://admin.fedoraproject.org/updates/'               #:
    resultsdb_server = 'http://127.0.0.1/resultsdb/api/v1.0/'               #:
    taskotron_master = 'http://127.0.0.1/taskmaster/'                       #:
    buildbot_task_step = 'runtask'                                          #:

    bodhi_posting_comments_span = 4320                                      #:
                                  # 3 days (3*24*60 = 4320)
    bodhi_request_max_retries = 3                                           #:

    tmpdir = '/var/tmp/taskotron'                                           #:
    logdir = '/var/log/taskotron'                                           #:
    log_name = 'taskotron.log'
    '''name of the main log file in :attr:`logdir`'''

    log_level_stream = 'INFO'                                               #:
    log_level_file = 'DEBUG'                                                #:

    log_file_enabled = False                                                #:

    fas_username = 'taskotron'                                              #:
    fas_password = ''                                                       #:


    def __str__(self):
        ''' Make this object more readable when printing '''
        return '<%s: %s>' % (self.__class__.__name__,
                             pprint.pformat(vars(self)))


class ProductionConfig(Config):
    '''Configuration for production profile. Inherits values from
    :class:`Config` and overrides some. Read Config documentation.'''

    profile = ProfileName.PRODUCTION                                        #:
    reports_enabled = True                                                  #:

    log_level_stream = 'INFO'                                               #:
    log_level_file = 'DEBUG'                                                #:

    log_file_enabled = True                                                 #:


class TestingConfig(Config):
    '''Configuration for testing suite profile. Inherits values from
    :class:`Config` and overrides some. Read Config documentation.'''

    profile = ProfileName.TESTING                                           #:

    tmpdir = '/var/tmp/taskotron-test/tmp'                                  #:
    logdir = '/var/tmp/taskotron-test/log'                                  #:

    log_level_stream = 'DEBUG'                                              #:
    log_level_file = 'DEBUG'                                                #:
