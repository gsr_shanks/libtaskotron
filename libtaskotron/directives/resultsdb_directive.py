# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing
from __future__ import absolute_import

DOCUMENTATION = """
module: resultsdb_directive
short_description: send task results to ResultsDB or check TAP correctness
description: |
  Send check output to `ResultsDB <https://fedoraproject.org/wiki/ResultsDB>`_.

  If reporting to ResultsDB is disabled in the config (`report_to_resultsdb`
  option, disabled by default for the development profile), the directive at
  least checks whether the check output is in valid `TAP13 format`_ and logs the
  details that would have been reported. For this reason, it's useful to use
  resultsdb directive always, even when you don't have any ResultsDB server
  configured.

  .. _TAP13 format: http://testanything.org/tap-version-13-specification.html
parameters:
  results:
    required: true
    description: The output of the check in TAP13 format
    type: str
returns: |
  A list of HTTP response objects, one for every HTTP request sent to create a
  result in ResultsDB (i.e. there should be the same number of responses as
  there are results in ``results`` parameter). The response objects are created
  by :meth:`requests.Response.json`, i.e. they are python dictionaries created
  from ResultsDB's response JSON object.
raises: |
  * :class:`.TaskotronDirectiveError`: if ``results`` is not in a valid TAP13
    format
  * :class:`resultsdb_api.ResultsDBapiException`: for any errors coming from
    the ResultsDB server
version_added: 0.4
"""

EXAMPLES = """
These two actions first run ``run_rpmlint.py`` and export its TAP13 output to
``${rpmlint_output}``, and then feed this TAP13 output to ``resultsdb``
directive::

    - name: run rpmlint on downloaded rpms
      python:
          file: run_rpmlint.py
          callable: run
          workdir: ${workdir}
      export: rpmlint_output

    - name: report results to resultsdb
      resultsdb:
          results: ${rpmlint_output}

If ResultsDB reporting is configured in the config file, it will get saved on
the ResultsDB server, otherwise only TAP13 compliance will get checked and some
summary will be printed out into the log, like this::

    [libtaskotron:resultsdb_directive.py:143] 2014-06-24 13:55:27 INFO    TAP is OK.
    [libtaskotron:resultsdb_directive.py:144] 2014-06-24 13:55:27 INFO    Reporting to ResultsDB is disabled.
    [libtaskotron:resultsdb_directive.py:145] 2014-06-24 13:55:27 INFO    Once enabled, the following would be reported:
    [libtaskotron:resultsdb_directive.py:146] 2014-06-24 13:55:27 INFO    <CheckDetail: {'_outcome': 'PASSED',
     'item': 'xchat-2.8.8-21.fc20.x86_64.rpm',
     'keyvals': {},
     'output': '<stripped out>',
     'report_type': 'koji_build',
     'summary': 'RPMLINT PASSED for xchat-2.8.8-21.fc20.x86_64.rpm'}>
    <CheckDetail: {'_outcome': 'PASSED',
     'item': 'xchat-tcl-2.8.8-21.fc20.x86_64.rpm',
     'keyvals': {},
     'output': '<stripped out>',
     'report_type': 'koji_build',
     'summary': 'RPMLINT PASSED for xchat-tcl-2.8.8-21.fc20.x86_64.rpm'}>
"""

from libtaskotron.directives import BaseDirective

from libtaskotron import check
from libtaskotron import config
from libtaskotron import buildbot_utils

from libtaskotron.exceptions import TaskotronDirectiveError, TaskotronValueError
from libtaskotron.logger import log

import resultsdb_api

directive_class = 'ResultsdbDirective'

class ResultsdbDirective(BaseDirective):

    def __init__(self, resultsdb = None):
        self.resultsdb = resultsdb

        conf = config.get_config()
        self.masterurl = conf.taskotron_master
        self.task_stepname = conf.buildbot_task_step

        if self.resultsdb is None:
            self.resultsdb = resultsdb_api.ResultsDBapi(conf.resultsdb_server)

    def ensure_testcase_exists(self, name):
        """ Make sure that the testcase exists in resultsdb, otherwise create
        the testcase using a dummy url as a reference

        :param str name: name of the testcase to check for
        """

        try:
            self.resultsdb.get_testcase(name)
            return
        except resultsdb_api.ResultsDBapiException, e:
            if e.message != 'Testcase not found':
                raise e

        # since the testcase doesn't exist, create it with a dummy value for url
        # it can be updated later when it's not blocking results reporting
        dummyurl = 'http://faketestcasesRus.com/%s' % name
        self.resultsdb.create_testcase(name, dummyurl)

    def create_resultsdb_job(self, name, refurl=None):
        """ Create a job in resultsdb for reporting results against

        :param str name: name of job to report against
        :returns: dict of rendered json results
        """

        # this is completly hacky for now since the runner doesn't have a
        # good way to get the actual refurl. This will be addressed in T122 and
        # T145

        url = 'http://not-a-real-domain.com/noworky'
        if refurl is not None:
            url = refurl

        jobdata = self.resultsdb.create_job(url, status='SCHEDULED', name=name)
        self.resultsdb.update_job(id=jobdata['id'], status='RUNNING')

        return jobdata

    def complete_resultsdb_job(self, jobid):
        """ Change the resultsdb job to a status of ``COMPLETED``, indicating
        that the reporting is complete.

        :param int jobid: The resultsdb jobid to be modified
        :returns: json representation of returned data from the resultsdb instance
        """
        return self.resultsdb.update_job(jobid, status='COMPLETED')

    def process(self, input_data, env_data):
        #TODO: Replace with proper location and adjust exc. text
        # we're creating the jobid in the directive for now, so adjust the check

        if 'checkname' not in env_data:
            raise TaskotronDirectiveError("The resultsdb directive requires "\
                    "resultsdb_job_id and checkname.")

        # checking if reporting is enabled is done after importing tap which
        # serves as validation of input results
        try:
            check_details = check.import_TAP(input_data['results'])
            log.debug("TAP output is OK.")
        except TaskotronValueError as e:
            raise TaskotronDirectiveError("Failed to load 'results': %s"  % e.message)

        conf = config.get_config()
        if not (conf.reporting_enabled and conf.report_to_resultsdb):
            log.info("Reporting to ResultsDB is disabled. Once enabled, the "
                     "following would get reported:\n%s" %
                     report_summary(input_data['results'], env_data['checkname']))
            log.info('Hint: Enabling debug output allows you to see unstripped '
                     'values during variable export.')
            return

        # for now, we're creating the resultsdb job at reporting time
        # the job needs to be 'RUNNING' in order to append any results
        job_url, log_url = buildbot_utils.get_urls(env_data['jobid'],
                                                   self.masterurl,
                                                   self.task_stepname)
        job_data = self.create_resultsdb_job(env_data['checkname'],
                                             refurl=job_url)

        self.ensure_testcase_exists(env_data['checkname'])

        output = []

        log.info('Posting %s results to ResultsDB...' % len(check_details))
        for detail in check_details:
            try:
                result = self.resultsdb.create_result(
                        job_id = job_data['id'],
                        testcase_name = env_data['checkname'],
                        outcome = detail.outcome,
                        summary = detail.summary or None,
                        log_url = log_url,
                        item = detail.item,
                        type = detail.report_type,
                        **detail.keyvals)
                output.append(result)

            except resultsdb_api.ResultsDBapiException, e:
                log.error(e)
                log.error("Failed to store ResulsDB: `%s` `%s` `%s`",
                            detail.item, env_data['checkname'], detail.outcome)

        # move job to completed
        self.complete_resultsdb_job(job_data['id'])

        return output


def report_summary(tap, checkname):
    '''Create a pretty summary of what will/would be reported into ResultsDB.
    The summary is based on TAP output, with the TAP header stripped out and
    most of the check output stripped out.

    :param str tap: TAP-formatted results
    :param str checkname: the name of the check
    :return: summary in plain text
    :rtype: str
    :raise TaskotronValueError: if `tap` has invalid format
    '''

    check_details = check.import_TAP(tap)
    summary = []

    for detail in check_details:
        # keep just the first and the last line of output
        lines = ('\n'.join(detail.output)).splitlines()
        if len(lines) > 3:
            detail.output = [lines[0],
                             '<... %s lines stripped out ...>' % (len(lines) - 2),
                             lines[-1]]
        out = check.export_TAP(detail, checkname=checkname)
        # strip "TAP version 13\n1..1" (first two lines)
        out = '\n'.join(out.splitlines()[2:])
        summary.append(out)

    return '\n'.join(summary)
