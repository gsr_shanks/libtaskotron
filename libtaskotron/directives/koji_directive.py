# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing
from __future__ import absolute_import

DOCUMENTATION = """
module: koji_directive
short_description: download builds and tags from Koji
description: |
  The koji directive interfaces with `Koji <http://koji.fedoraproject.org/>`_
  to facilitate various Koji actions. You can either download all RPMs from
  a specific build, or you can download all RPMs from all builds belonging
  to a specific Koji tag.
parameters:
  action:
    required: true
    description: choose whether to download a single build (``download`` value)
      or all builds belonging to a Koji tag (``download_tag`` value)
    type: str
    choices: [download, download_tag]
  arch:
    required: true
    description: |
      a list of architectures for which to download RPMs for the requested
      build/tag. If you want to download RPMs for all arches, use ``['all']``.

      Note: ``noarch`` RPMs are always automatically downloaded even when not
      requested, unless ``arch=[]`` and ``src=True``.
    type: list of str
    choices: [supported architectures, ['all']]
  koji_build:
    required: true
    description: |
      N(E)VR of a Koji build to download. Only required when
      ``action="download"``. Example: ``xchat-2.8.8-21.fc20``
    type: str
  koji_tag:
    required: true
    description: |
      name of a Koji tag to download all builds from. Only required when
      ``action="download_tag"``. Example: ``f20-updates-pending``
    type: str
  src:
    required: false
    description: download also ``src`` RPM files
    type: bool
    default: False
  target_dir:
    required: false
    description: directory into which to download builds
    type: str
    default: ${workdir}
returns: |
  A dictionary containing following items:

  * `downloaded_rpms`: (list of str) a list of local filenames of the downloaded
    RPMs
raises: |
  * :class:`.TaskotronRemoteError`: if downloading failed
  * :class:`.TaskotronValueError`: if ``arch=[]`` and ``src=False``, therefore
    there is nothing to download
version_added: 0.4
"""

EXAMPLES = """
Rpmlint needs to download a specific build from Koji, all architectures
including src.rpm::

  - name: download rpms from koji
    koji:
      action: download
      koji_build: ${koji_build}
      arch: ['all']
      src: True

Depcheck needs to download all builds in a specific Koji tag for the current
architecture::

  - name: download koji tag
    koji:
        action: download_tag
        koji_tag: ${koji_tag}
        arch: ${arch}
        target_dir: ${workdir}/downloaded_tag/
"""

from libtaskotron.koji_utils import KojiClient
from libtaskotron.directives import BaseDirective
from libtaskotron import rpm_utils
import libtaskotron.exceptions as exc

directive_class = 'KojiDirective'

class KojiDirective(BaseDirective):

    def __init__(self, koji_session=None):
        super(KojiDirective, self).__init__()
        if koji_session is None:
            self.koji = KojiClient(koji_session)
        else:
            self.koji = koji_session

    def process(self, input_data, env_data):
        # process params
        valid_actions = ['download', 'download_tag']
        action = input_data['action']
        if not action in valid_actions:
            raise exc.TaskotronDirectiveError('%s is not a valid action for koji '
                                              'directive' % action)

        if not 'target_dir' in input_data:
            self.target_dir = env_data['workdir']
        else:
            self.target_dir = input_data['target_dir']

        if 'arch' not in input_data:
            detected_args = ', '.join(input_data.keys())
            raise exc.TaskotronDirectiveError(
                "The koji directive requires 'arch' as an argument. Detected "
                "arguments: %s" % detected_args)

        self.arches = input_data['arch']
        if self.arches and ('noarch' not in self.arches):
            self.arches.append('noarch')

        self.src = input_data.get('src', False)

        # download files
        output_data = {}

        if action == 'download':
            if 'koji_build' not in input_data:
                detected_args = ', '.join(input_data.keys())
                raise exc.TaskotronDirectiveError(
                    "The koji directive requires 'koji_build' for the 'download' "
                    "action. Detected arguments: %s" % detected_args)

            nvr = rpm_utils.rpmformat(input_data['koji_build'], 'nvr')

            output_data['downloaded_rpms'] = self.koji.get_nvr_rpms(nvr,
                self.target_dir, arches=self.arches, src=self.src)

        elif action == 'download_tag':
            if 'koji_tag' not in input_data:
                detected_args = ', '.join(input_data.keys())
                raise exc.TaskotronDirectiveError(
                    "The koji directive requires 'koji_tag' for the 'download_tag' "
                    "action. Detected arguments: %s" % detected_args)

            koji_tag = input_data['koji_tag']

            output_data['downloaded_rpms'] = self.koji.get_tagged_rpms(koji_tag,
                self.target_dir, arches=self.arches, src=self.src)

        return output_data
