# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing
from __future__ import absolute_import

DOCUMENTATION = """
module: yumrepoinfo_directive
short_description: translate Koji tags into YUM repository URLs
description: |
  Translate a Koji tag into a set of YUM repositories. This is useful when you
  want to work with YUM metadata for a specific Fedora repository, but you only
  know its Koji tag.
parameters:
  koji_tag:
    required: true
    description: |
      name of the Koji tag, e.g. ``f20``, ``f20-updates``,
      ``f20-updates-testing``, or ``rawhide``.

      If the tag ends with ``-pending`` suffix, this suffix is stripped (because
      pending tags are not real repositories), and the resulting tag is used.
    type: str
  arch:
    required: true
    description: list of architectures. In this list, all "meta architectures"
      like ``all``, ``src`` and ``noarch`` are ignored, and only a single "real
      architecture" is expected there (e.g. ``x86_64`` or ``i386``).
    type: list of str
returns: |
    A dictionary. One of the keys is ``koji_tag`` and URL for the respective
    repository is its value.

    If the repository in question has some parent repositories (e.g.
    ``f20-updates`` is a child of ``f20``), all its parents names and their
    repository URLs are returned in the dictionary as well as additional keys.
raises: |
  * :class:`.TaskotronDirectiveError`: if there are zero or more than one "real
    architectures" inside ``arch`` (e.g. both ``x86_64`` and ``i386``). Also
    when ``koji_tag`` is not found among known repositories.
version_added: 0.4
"""

EXAMPLES = """
For a koji tag received from the command line, get a list of its YUM
repositories and save it to a variable::

    - name: get yum repos
      yumrepoinfo:
          koji_tag: ${koji_tag}
          arch: ${arch}
      export: yumrepos

Now you can pass ``${yumrepos}`` dictionary to your python script and run your
check::

    - name: run my check
      python:
          file: my_check.py
          callable: run
          repos: ${yumrepos}
          arch: ${arch}
      export: my_check_output
"""

from libtaskotron.directives import BaseDirective
from libtaskotron import yumrepoinfo
from libtaskotron.exceptions import TaskotronDirectiveError
from libtaskotron.logger import log
from libtaskotron.arch_utils import Arches

directive_class = 'YumrepoinfoDirective'

class YumrepoinfoDirective(BaseDirective):

    def __init__(self, repoinfo = None):
        self.repoinfo = repoinfo

    def process(self, input_data, env_data):

        if 'koji_tag' not in input_data or 'arch' not in input_data:
            raise TaskotronDirectiveError("The yumrepoinfo directive requires "
                    "koji_tag and arch arguments.")


        arches = input_data['arch']

        processed_arches = [arch for arch in arches if arch in Arches.base]
        if len(processed_arches) == 0:
            raise TaskotronDirectiveError("No valid yumrepo arches supplied to "
                    "yumrepoinfo directive. Recieved %r" % arches)

        if len(processed_arches) > 1:
          raise TaskotronDirectiveError("Yumrepoinfo requires a single base "
                    "arch but multiple arches were submitted: %r" % arches)

        arch = processed_arches[0]

        koji_tag = input_data['koji_tag']

        output = {}

        if self.repoinfo is None:
            self.repoinfo = yumrepoinfo.get_yumrepoinfo(arch)

        if koji_tag.endswith('-pending'):
           koji_tag = koji_tag[:-len('-pending')]

        if koji_tag == 'rawhide':
            koji_tag = self.repoinfo.repo('rawhide')['tag']

        while koji_tag:
            repo = self.repoinfo.repo_by_tag(koji_tag)
            if repo is None:
                raise TaskotronDirectiveError('Repo with tag'\
                        '%r not found.' % koji_tag)

            output[repo['name']] = repo['url']
            koji_tag = repo['parent']

        log.debug("Found %s repos for %s: %r" % (len(output),
            input_data['koji_tag'], output))
        return output

