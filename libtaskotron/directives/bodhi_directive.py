# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing
from __future__ import absolute_import

DOCUMENTATION = """
module: bodhi_directive
short_description: download updates from Bodhi
description: |
  The bodhi directive interfaces with `Bodhi`_ to facilitate various actions.
  At the moment, the only action supported is downloading updates (i.e. all RPMs
  from all builds related to a specific update).

  .. _Bodhi: https://admin.fedoraproject.org/updates/
parameters:
  action:
    required: true
    description: specify action type. The only action available at the moment
      is ``download``.
    type: str
  bodhi_id:
    required: true
    description: Bodhi update ID to download. This is usually in the form of
      ``FEDORA-2014-7485`` or ``pykickstart-1.99.42-1.fc20,anaconda-20.23-1.fc20``.
    type: str
  arch:
    required: true
    description: |
      a list of architectures for which to download RPMs. In addition to
      architectures supported by Taskotron by default, ``src`` is supported as
      well.

      Note: ``noarch`` RPMs are always automatically downloaded even when not
      requested, unless ``src`` is the only architecture requested.
    type: list of str
returns: |
  A dictionary containing following items:

  * ``downloaded_rpms``: (list of str) a list of local filenames of the
    downloaded RPMs
raises: |
  * :class:`.TaskotronDirectiveError`: if no update with ``bodhi_id`` has been
    found
  * :class:`.TaskotronRemoteError`: if downloading failed
version_added: 0.4
"""

EXAMPLES = """
Download a Bodhi update provided on the command line and then check it with
rpmlint::

    - name: download update from Bodhi
      bodhi:
        action: download
        bodhi_id: ${bodhi_id}
        arch: ${arch}

    - name: run rpmlint on downloaded rpms
      python:
          file: run_rpmlint.py
          callable: run
          workdir: ${workdir}
      export: rpmlint_output
"""

import libtaskotron.bodhi_utils as bodhi
from libtaskotron.koji_utils import KojiClient
from libtaskotron.directives import BaseDirective
from libtaskotron.arch_utils import Arches
from libtaskotron.logger import log
from libtaskotron.exceptions import TaskotronDirectiveError

directive_class = 'BodhiDirective'

class BodhiDirective(BaseDirective):

    def __init__(self, bodhi_api=None, koji_session=None):

        if bodhi_api:
            self.bodhi_api = bodhi_api
        else:
            self.bodhi_api = bodhi.BodhiUtils()

        if koji_session:
            self.koji_session = koji_session
        else:
            self.koji_session = KojiClient()

    def action_download(self, update, arches, workdir):

        res = self.bodhi_api.query_update(update)

        if res is None:
            raise TaskotronDirectiveError("Update with id '%s' wasn't found"
                                    % update)

        nvrs = [build['nvr'] for build in res['builds']]

        downloaded_rpms = []

        for nvr in nvrs:
            res = self.koji_session.get_nvr_rpms(nvr, workdir, arches)
            downloaded_rpms.extend(res)

        return downloaded_rpms

    def process(self, input_data, env_data):
        output_data = {}

        valid_actions = ['download']

        action = input_data['action']

        if action not in valid_actions:
            raise TaskotronDirectiveError('%s is not a valid command for bodhi directive'
                                 % action)

        if action == 'download':
            if 'bodhi_id' not in input_data or 'arch' not in input_data:
                detected_args = ', '.join(input_data.keys())
                raise TaskotronDirectiveError("The bodhi directive 'download'"
                   " requires both 'bodhi_id' and"
                   " 'arch' arguments. Detected arguments: %s" % detected_args)

            workdir = env_data['workdir']
            update = input_data['bodhi_id']
            if 'all' in input_data['arch']:
                arches = Arches.base + ['noarch']
            else:
                arches = input_data['arch']
            if 'noarch' not in arches and arches != ['src']:
                arches.append('noarch')

            log.info("getting rpms for update %s (%s) and downloading to %s",
                      update, arches, workdir)

            output_data['downloaded_rpms'] = self.action_download(update, arches, workdir)

        return output_data
