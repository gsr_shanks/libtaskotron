# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from __future__ import absolute_import
import logging
import tempfile
import os.path
import argparse
import imp
import copy
import collections

import libtaskotron
from libtaskotron import taskformula
from libtaskotron import logger
from libtaskotron import python_utils
from libtaskotron import config
from libtaskotron.logger import log
from libtaskotron.exceptions import TaskotronYamlError


# The list of accepted item types on the command line (--type option)
_ITEM_TYPES = ["bodhi_id", "koji_build", "koji_tag"]


class Runner(object):
    def __init__(self, taskdata, argdata, workdir=None):
        self.taskdata = taskdata
        self.envdata = argdata
        self.working_data = {}
        self.directives = {}
        self.workdir = workdir

    def run(self):
        self._validate_input()

        if not self.workdir:  # create temporary workdir if needed
            self.workdir = tempfile.mkdtemp(prefix="task-",
                                            dir=config.get_config().tmpdir)
        log.debug("Current workdir: %s", self.workdir)
        self.envdata['workdir'] = self.workdir
        self.envdata['checkname'] = self.taskdata['name']

        #override variable values
        for var, val in self.envdata['override']:
            log.debug("Overriding variable %s, new value: %s", var, val)
            self.envdata[var] = eval(val, {}, {})

        self.do_actions()

    def _load_directive(self, directive_name, directive_dir=None):
        # look in default path if nothing is specified
        if not directive_dir:
            directive_dir = os.path.join(os.path.dirname(__file__),
                                         'directives')

        real_name = "%s_directive" % directive_name
        directive_file = os.path.join(directive_dir, '%s.py' % real_name)

        if not os.path.exists(directive_file):
            raise TaskotronYamlError("Directive %s not found in directory %s" %
                            (directive_name, directive_dir))

        loaded_directive = imp.load_source(real_name, directive_file)
        self.directives[directive_name] = loaded_directive

    def _render_action(self, action):
        '''Take an action and replace all included variables with actual values
        from :attr:`env_data` and :attr:`working_data`. See :meth:`do_actions`
        to see how an action looks like.

        :param dict action: An action specification parsed from the task formula
        :return: a rendered action
        :rtype: dict
        '''
        # copy the input so that we don't disrupt what we're processing
        rendered_action = copy.deepcopy(action)

        variables = copy.deepcopy(self.envdata)
        variables.update(self.working_data)
        taskformula.replace_vars_in_action(rendered_action, variables)

        return rendered_action

    def _extract_directive_from_action(self, action):
        for key in action:
            if key not in ['name', 'export']:
                return key
        raise TaskotronYamlError('no directive found in action %s' %
                                 str(action))

    def do_single_action(self, action):
        '''Execute a single action from the task. See :meth:`do_actions` to see
        how an action looks like.

        :param dict action: An action specification parsed from the task formula
        '''
        directive_name = self._extract_directive_from_action(action)

        rendered_action = self._render_action(action)

        log.debug("Executing directive: %s", directive_name)
        self._load_directive(directive_name)

        directive_object = self.directives[directive_name]
        directive_classname = directive_object.directive_class

        directive_callable = getattr(directive_object, directive_classname)()

        output = directive_callable.process(rendered_action[directive_name],
                                            self.envdata)

        if 'export' in action:
            self.working_data[action['export']] = output
            log.debug("Variable ${%s} was exported with value:\n%s" %
                      (action['export'], output))

    def do_actions(self):
        '''Sequentially run all actions for a task. An 'action' is a single step
        under the ``actions:`` key. An example action looks like::

          - name: download rpms from koji
            koji:
              action: download
              koji_build: $koji_build
              arch: $arch
        '''
        if 'actions' not in self.taskdata or not self.taskdata['actions']:
            raise TaskotronYamlError("At least one task should be specified"
                                     " in input formula")

        for action in self.taskdata['actions']:
            self.do_single_action(action)

    def _validate_input(self):
        if 'input' not in self.taskdata:
            return

        if not isinstance(self.taskdata['input'], collections.Mapping):
            raise TaskotronYamlError("Input yaml should contain correct 'input'"
                "section (a mapping). Yours was: %s" % type(
                self.taskdata['input']))

        required_args = self.taskdata['input'].get('args', None)

        if not python_utils.iterable(required_args):
            raise TaskotronYamlError("Input yaml should contain correct 'args' "
                "section (an iterable). Yours was: %s" % type(required_args))

        for arg in required_args:
            if not arg in self.envdata:
                raise TaskotronYamlError("Required input arg '%s' "
                                         "was not defined" % arg)

    def _validate_env(self):
        # TODO: implement this
        raise NotImplementedError("Environment validation is not"
                                  " yet implemented")


def get_argparser():
    parser = argparse.ArgumentParser()
    parser.add_argument("task", nargs=1, help="task to run")
    parser.add_argument("-a", "--arch",
        choices=["i386", "x86_64", "armhfp", "noarch"],
        action='append',
        help="architecture specifying the item to be checked. If omitted, "
             "defaults to noarch.")
    parser.add_argument("-i", "--item", help="item to be checked")
    parser.add_argument("-t", "--type",
                        choices=_ITEM_TYPES,
                        help="type of --item argument")
    parser.add_argument("-j", "--jobid", default="-1",
                        help="optional job identifier used to render log urls")
    parser.add_argument("-d", "--debug", action="store_true",
                        help="Enable debug output "
                             "(set logging level to 'DEBUG')")
    parser.add_argument("--override", action="append", default=[],
                        metavar='VAR=VALUE',
                        help="override internal variable values used in runner "
                             "and the task formula. Value itself is evaluated "
                             "by eval(). This option can be used multiple times. "
                             "Example: --override \"workdir='/some/dir/'\"")

    return parser


def process_args(args):
    """ Processes raw input args and converts them into specific data types that
    can be used by tasks. This includes e.g. creating new args based on
    (item, item_type) pairs, or adjusting selected architecture.

    :param dict args: dictionary of raw arguments
    :returns: dict of args with appended specific data
    """

    # process item + type
    if args['type'] in _ITEM_TYPES:
        args[args['type']] = args['item']

    # process arch
    if args['arch'] is None:
        args['arch'] = ['noarch']

    # using list of tuples instead of dictionary so the args['override'] is
    # still list at the end of process_args() - for better testability
    override = []
    for var in args['override']:
        name = var.split('=')[0]
        value = var.split('=')[1]
        override.append((name, value))
    args['override'] = override

    return args


def main():
    # Preliminary initialization of logging, so all messages before regular
    # initialization can be logged to stream.
    # FIXME: Remove this once this ticket is resolved:
    # https://phab.qadevel.cloud.fedoraproject.org/T273
    logger.init_prior_config()

    log.debug('Using libtaskotron %s', libtaskotron.__version__)

    # parse cmdline
    parser = get_argparser()
    args = parser.parse_args()
    log.debug('Parsed arguments: %s', args)

    # full logging initialization
    level_stream = logging.DEBUG if args.debug else None
    logger.init(level_stream=level_stream)

    arg_data = process_args(vars(args))
    arg_data['taskfile'] = args.task[0]

    # parse task formula
    task_data = taskformula.parse_yaml_from_file(arg_data['taskfile'])
    if not task_data:
        raise TaskotronYamlError('Input file should not be empty')

    # run the task
    task_runner = Runner(task_data, arg_data)
    task_runner.run()

    # finalization
    log.info('Check execution finished.')
