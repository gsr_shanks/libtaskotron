# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

''' Utility methods related to RPM '''

from __future__ import absolute_import
import os
import hawkey

from libtaskotron import exceptions as exc


def rpmformat(rpmstr, fmt='nvr', end_arch=False):
    '''
    Parse and convert an RPM package version string into a different format.
    String identifiers: N - name, E - epoch, V - version, R - release, A -
    architecture.

    :param str rpmstr: string to be manipulated in a format of N(E)VR
                       (``foo-1.2-3.fc20`` or ``bar-4:1.2-3.fc20``) or N(E)VRA
                       (``foo-1.2-3.fc20.x86_64`` or ``bar-4:1.2-3.fc20.i686``)
    :param str fmt: desired format of the string to be returned. Allowed options
                    are: ``nvr``, ``nevr``, ``nvra``, ``nevra``, ``n``, ``e``,
                    ``v``, ``r``, ``a``. If arch is not present in ``rpmstr``
                    but requested in ``fmt``, ``noarch`` is used. Epoch is
                    provided only when specifically requested (e.g.
                    ``fmt='nevr'``) **and** being non-zero; otherwise it's
                    supressed (the only exception is ``fmt='e'``, where you
                    receive ``0`` for zero epoch).
    :param bool end_arch: set this to ``True`` if ``rpmstr`` ends with an
                          architecture identifier (``foo-1.2-3.fc20.x86_64``).
                          It's not possible to reliably distinguish that case
                          automatically.
    :return: string based on the specified format, or integer if ``fmt='e'``
    :raise TaskotronValueError: if ``fmt`` value is not supported
    '''
    fmt = fmt.lower()
    supported_formats = ['nvr', 'nevr', 'nvra', 'nevra',
                         'n', 'e', 'v', 'r', 'a']
    if fmt not in supported_formats:
        raise exc.TaskotronValueError("Format '%s' not in supported formats "
            "(%s)" % (fmt, ', '.join(supported_formats)))

    # add arch if not present
    if not end_arch:
        rpmstr += '.noarch'

    # split rpmstr
    nevra = hawkey.split_nevra(rpmstr)

    # return simple fmt
    if len(fmt) == 1:
        return {'n': nevra.name,
                'e': nevra.epoch,
                'v': nevra.version,
                'r': nevra.release,
                'a': nevra.arch}[fmt]

    # return complex fmt
    evr = nevra.evr()
    # supress epoch if appropriate
    if 'e' not in fmt or nevra.epoch == 0:
        evr = evr[evr.find(':')+1:]  # remove 'epoch:' from the beginning

    result = '%s-%s' % (nevra.name, evr)

    # append arch if requested
    if 'a' in fmt:
        result += '.' + nevra.arch

    return result


def cmpNEVR(nevr1, nevr2):
    '''Compare two RPM version identifiers in NEVR format.

    :param str nevr1: RPM identifier in N(E)VR format
    :param str nevr2: RPM identifier in N(E)VR format
    :return: ``-1``/``0``/``1`` if ``nevr1 < nevr2`` / ``nevr1 == nevr2`` /
             ``nevr1 > nevr2``
    :rtype: int
    :raise TaskotronValueError: if name in ``nevr1`` doesn't match name in
                                ``nevr2``
    '''
    rpmver1 = hawkey.split_nevra(nevr1 + '.noarch')
    rpmver2 = hawkey.split_nevra(nevr2 + '.noarch')

    if rpmver1.name != rpmver2.name:
        raise exc.TaskotronValueError("Name in nevr1 doesn't match name in "
            "nevr2: %s, %s" % (nevr1, nevr2))

    # sack is needed for the comparison, because it can be influence the
    # comparison (for example epoch can be set to be ignored). A default empty
    # sack should match Fedora customs
    sack = hawkey.Sack()

    return rpmver1.evr_cmp(rpmver2, sack)
