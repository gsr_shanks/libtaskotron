# -*- coding: utf-8 -*-
# Copyright 2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Functional tests for libtaskotron/koji_utils.py'''

from libtaskotron import koji_utils

from dingus import Dingus


class TestKojiClient():

    def setup_method(self, method):
        self.ref_nvr = 'foo-1.2-3.fc99'
        self.ref_arch = 'noarch'
        self.ref_name = 'foo'
        self.ref_version = '1.2'
        self.ref_release = '3.fc99'
        self.ref_buildid = 123456

        self.ref_build = {'package_name': self.ref_name,
                          'version': self.ref_version,
                          'release': self.ref_release,
                          'id': self.ref_buildid}

    def test_handle_norpms_in_build(self, tmpdir):
        """ This tests to make sure that missing rpms in a build are handled
        gracefully during download so that execution isn't stopped when a build
        is missing an rpm """

        rpmdir = tmpdir.mkdir("rpmdownload")
        stub_koji = Dingus(getBuild__returns = self.ref_build)
        test_koji = koji_utils.KojiClient(stub_koji)

        test_koji.get_nvr_rpms(self.ref_nvr, str(rpmdir), arches=[self.ref_arch])
