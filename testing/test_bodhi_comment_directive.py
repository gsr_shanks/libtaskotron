# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

"""Unit tests for libtaskotron/directives/resultsdb_directive.py"""

import pytest
import datetime
import itertools
from dingus import Dingus
from copy import deepcopy

from libtaskotron.directives import bodhi_comment_directive
from libtaskotron.exceptions import TaskotronDirectiveError, TaskotronValueError

from libtaskotron import check
from libtaskotron import config


@pytest.mark.usefixtures('setup')
class TestBodhiCommentReport():

    @pytest.fixture
    def setup(self, monkeypatch):
        '''Run this before every test invocation'''
        monkeypatch.setattr(config, '_config', None)
        Config = config.get_config()
        Config.reporting_enabled = True
        Config.report_to_bodhi = True

        self.ref_job_id = 123
        self.ref_log_url = '%s/builders/all/builds/%d/steps/runtask/logs/stdio'\
                            % (Config.taskotron_master, self.ref_job_id)

        self.comment = {
                "author": Config.fas_username,
                "timestamp": "2014-01-31 10:11:12",
                "text": "Taskotron: depcheck test PASSED on x86_64. Result log: %s" % self.ref_log_url,
                }

        self.update = {
                'request': 'stable',
                'comments': [self.comment],
                'date_modified': "2014-01-31 10:00:00"
                }

        self.stub_bodhi = Dingus(query_update__returns = self.update)
        self.directive = bodhi_comment_directive.BodhiCommentDirective(self.stub_bodhi)

        self.cd = check.CheckDetail(
                    item = 'update_title',
                    report_type = check.ReportType.BODHI_UPDATE,
                    outcome = 'PASSED',
                )


        tap = check.export_TAP(self.cd)

        self.ref_input = {
                'results': tap,
                'doreport': 'onchange',
                }
        self.ref_envdata = {
                'checkname': 'depcheck',
                'jobid': 'all/%d' % self.ref_job_id,
                }

    def test_config_reporting_disabled(self):
        """Checks config options can disable reporting."""
        Config = config.get_config()

        for r_e, r_t_b in ((False, False), (False, True), (True, False)):
           Config.reporting_enabled = r_e
           Config.report_to_bodhi = r_t_b

           assert self.directive.process(self.ref_input, self.ref_envdata) is None

    def test_missing_arguments_in_inputdata(self):
        """Check if arguments raise exception"""
        with pytest.raises(TaskotronDirectiveError):
            input_data = deepcopy(self.ref_input)
            del(input_data['results'])
            self.directive.process(input_data, self.ref_envdata)

        with pytest.raises(TaskotronDirectiveError):
            input_data = deepcopy(self.ref_input)
            del(input_data['doreport'])
            self.directive.process(input_data, self.ref_envdata)

    def test_wrong_doreport_value(self):
        """Checks whether unsupported doreport value"""
        with pytest.raises(TaskotronDirectiveError):
            input_data = deepcopy(self.ref_input)
            input_data['doreport']  = 'FooBar'
            self.directive.process(input_data, self.ref_envdata)

    def test_missing_checkname(self, monkeypatch):
        """Checks if missing checkname raises exception"""
        with pytest.raises(TaskotronDirectiveError):
            envdata = deepcopy(self.ref_envdata)
            del(envdata['checkname'])

            self.directive.process(self.ref_input, envdata)

    def test_failed_tap_import(self, monkeypatch):
        """Checks if failed TAP import raises exception"""
        def mock_raise(self, *args, **kwargs):
            raise TaskotronValueError("Testing Error")

        monkeypatch.setattr(check, 'import_TAP', mock_raise)

        with pytest.raises(TaskotronDirectiveError):
            self.directive.process(self.ref_input, self.ref_envdata)


    def test_parse_result_from_comment(self):
        """Checks parsing of the bodhi comments"""
        output = bodhi_comment_directive._parse_result_from_comment(self.comment)

        assert output['time'] == datetime.datetime(2014, 1, 31, 10, 11, 12)
        assert output['testname'] == 'depcheck'
        assert output['result'] == 'PASSED'
        assert output['arch'] == 'x86_64'

    def test_already_commented_nonexistent_update(self):
        """Checks whether non-existing updates throws
        an error in _already_commented."""
        stub_bodhi = Dingus(query_update__returns = None)

        with pytest.raises(TaskotronValueError):
            bodhi_comment_directive._already_commented(stub_bodhi, '', '', '')


    def test_already_commented(self):
        """Tests general _already_commented behaviour."""

        # Test comment found
        outcome = bodhi_comment_directive._already_commented(None, self.update, 'depcheck', 'x86_64')
        assert outcome == ('PASSED', self.comment['timestamp'])

        # Test no comment
        update = deepcopy(self.update)
        update['comments'] = []
        outcome = bodhi_comment_directive._already_commented(None, update, 'depcheck', 'x86_64')
        assert outcome == ('', '')

        # Test modified after commented
        update = deepcopy(self.update)
        update['date_modified'] = "2014-01-31 15:00:00"
        outcome = bodhi_comment_directive._already_commented(None, update, 'depcheck', 'x86_64')
        assert outcome == ('', '')

    def test_is_comment_needed(self):
        """Checks whether _is_comment_needed returns proper results in
        various situations."""
        # Test first comment
        outcome = bodhi_comment_directive._is_comment_needed(None, None, None, None)
        assert outcome == True

        # Test different result
        outcome = bodhi_comment_directive._is_comment_needed('PASSED', None, 'FAILED', None)
        assert outcome == True

        # Test same result, but not FAILED
        outcome = bodhi_comment_directive._is_comment_needed('PASSED', None, 'PASSED', None)
        assert outcome == False

    def test_is_comment_needed_decide_repost(self, monkeypatch):
        """Checks whether time_span has an impact on comment posting."""
        date_utcnow = datetime.datetime(2014, 01, 10, 12, 05)
        date_posted = datetime.datetime(2014, 01, 10, 12, 00)

        class FakeDatetime(datetime.datetime):
            @classmethod
            def utcnow(cls):
                return date_utcnow


        monkeypatch.setattr(datetime, 'datetime', FakeDatetime)

        # Test same result, FAILED, delta < time_span
        time_span = 1000
        comment_time = date_posted.strftime('%Y-%m-%d %H:%M:%S')
        outcome = bodhi_comment_directive._is_comment_needed('FAILED', comment_time, 'FAILED', time_span)
        assert outcome == False

        # Test same result, FAILED, delta > time_span
        time_span = 0
        comment_time = date_posted.strftime('%Y-%m-%d %H:%M:%S')
        outcome = bodhi_comment_directive._is_comment_needed('FAILED', comment_time, 'FAILED', time_span)
        assert outcome == True

    def test_post_testresult_params_and_config(self):
        """Checks whether missing parameters/config values terminate
        _post_testresults."""
        # Test empty parameters
        outcome = bodhi_comment_directive._post_testresult(Dingus(),
                update = None,
                testname = None,
                result = None,
                url = None)
        assert outcome == False

        # Test configuration
        Config = config.get_config()
        Config.fas_username = None
        Config.fas_password = None
        outcome = bodhi_comment_directive._post_testresult(Dingus(),
                update = True,
                testname = True,
                result = True,
                url = True)
        config._config = None
        assert outcome == False

    @pytest.fixture
    def prepare_for_bodhi_comment(self, monkeypatch):
        """Sets up environment for fake bodhi comment calls"""
        Config = config.get_config()
        Config.fas_password = "secret!"

        stub_already_commented = lambda *args, **kwargs: ('', '')
        stub_return_true = lambda *args, **kwargs: True
        stub_return_false = lambda *args, **kwargs: False

        monkeypatch.setattr(bodhi_comment_directive, '_already_commented', stub_already_commented)
        monkeypatch.setattr(bodhi_comment_directive, '_is_comment_needed', stub_return_true)
        monkeypatch.setattr(bodhi_comment_directive, '_is_comment_email_needed', stub_return_false)


    def test_post_testresult_comment_not_needed(self, monkeypatch, prepare_for_bodhi_comment):
        """Checks the behaviour when comment is not needed."""
        self.stub_bodhi.client = Dingus('bodhi_api.bodhi')

        monkeypatch.setattr(bodhi_comment_directive, '_is_comment_needed', lambda *a, **k: False)

        outcome = bodhi_comment_directive._post_testresult(self.stub_bodhi,
                update = True,
                testname = True,
                result = True,
                url = True,
                doreport = 'onchange')
        assert outcome == True
        assert len(self.stub_bodhi.client.calls()) == 0

    def test_post_testresult(self, prepare_for_bodhi_comment):
        """Checks whether _post_testresult maps arguments correctly to
        the bodhi comment call."""
        ref_comment = "Taskotron: depcheck test PASSED on noarch. "\
                      "Result log: %s (results are informative only)" % self.ref_log_url
        self.stub_bodhi.client = Dingus('bodhi_api.bodhi')

        outcome = bodhi_comment_directive._post_testresult(self.stub_bodhi,
                update =  "update_title",
                testname = "depcheck",
                result = "PASSED",
                url = self.ref_log_url)

        # Select the first call of "comment" method.
        call = [call for call in self.stub_bodhi.client.calls() if call[0] == 'comment'][0]
        # Select the positional arguments of that call
        call_data = call[1]

        assert call_data[0] == "update_title"
        assert call_data[1] == ref_comment
        assert call_data[2] == 0
        assert call_data[3] == False

    def test_tap_mapping(self, prepare_for_bodhi_comment):
        """Checks whether TAP output is mapped correctly to
        the bodhi comment call."""
        ref_comment = "Taskotron: depcheck test PASSED on noarch. "\
                      "Result log: %s (results are informative only)" % self.ref_log_url
        self.stub_bodhi.client = Dingus('bodhi_api.bodhi')

        self.directive.process(self.ref_input, self.ref_envdata)

        # Select the first call of "comment" method.
        call = [call for call in self.stub_bodhi.client.calls() if call[0] == 'comment'][0]
        # Select the positional arguments of that call
        call_data = call[1]

        assert call_data[0] == "update_title"
        assert call_data[1] == ref_comment
        assert call_data[2] == 0
        assert call_data[3] == False

    def test_is_comment_email_needed(self, monkeypatch):
        def mock_BodhiUpdateState_factory(current_state, new_state):

            class MockBodhiUpdateState(Dingus):
                def __init__(self, *args, **kwargs):
                    Dingus.__init__(self, *args, **kwargs)
                    self.state = current_state

                def add_result(self, *args, **kwargs):
                    self.state = new_state

                def get_state(self):
                    return self.state

            return MockBodhiUpdateState

        PASS = bodhi_comment_directive.PASS
        FAIL = bodhi_comment_directive.FAIL
        INCOMPLETE = bodhi_comment_directive.INCOMPLETE
        UNKNOWN = bodhi_comment_directive.UNKNOWN


        # if new_state is PASS and the current_state was not 'FAIL', do not send email
        for current_state in (PASS, INCOMPLETE, UNKNOWN):
            mock_bus = mock_BodhiUpdateState_factory(current_state, PASS)
            monkeypatch.setattr(bodhi_comment_directive, 'BodhiUpdateState', mock_bus)

            outcome = bodhi_comment_directive._is_comment_email_needed(None, [], None, None)
            assert outcome == False

        # If current_state is FAIL, send email
        for new_state in (PASS, FAIL, INCOMPLETE, UNKNOWN):
            mock_bus = mock_BodhiUpdateState_factory(FAIL, new_state)
            monkeypatch.setattr(bodhi_comment_directive, 'BodhiUpdateState', mock_bus)

            outcome = bodhi_comment_directive._is_comment_email_needed(None, [], None, None)
            assert outcome == True

        # If new_state is not PASS and current_state is not FAIL, send email
        new_states = (FAIL, INCOMPLETE, UNKNOWN)
        current_states = (PASS, INCOMPLETE, UNKNOWN)

        for current_state, new_state in itertools.product(current_states, new_states):
            mock_bus = mock_BodhiUpdateState_factory(current_state, new_state)
            monkeypatch.setattr(bodhi_comment_directive, 'BodhiUpdateState', mock_bus)

            outcome = bodhi_comment_directive._is_comment_email_needed(None, [], None, None)
            assert outcome == True


