# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Unit tests for libtaskotron/bodhi_utils.py'''

import pytest
from dingus import Dingus
from bunch import Bunch
from fedora.client import ServerError

from libtaskotron import bodhi_utils
from libtaskotron import exceptions as exc


# http://stackoverflow.com/questions/3190706/nonlocal-keyword-in-python-2-x
def create_failing_server(response):
    first_call = {"value": True}

    def multicall(*args, **kwargs):
        if first_call["value"]:
            first_call["value"] = False
            raise ServerError("nonexist", 500, "Internal Server Error")
        else:
            return response

    return multicall


def raise_500_server_error(*args, **kwargs):
    raise ServerError("nonexist", 500, "Internal Server Error")


def raise_404_not_found(*args, **kwargs):
    raise ServerError("nonexist", 404, "Not found")


class TestQueryUpdate():
    '''Test query_update()'''

    def setup_method(self, method):
        '''Run this before every test invocation'''
        self.ref_bodhi_id = 'FEDORA-1234-56789'
        self.ref_update = {'title': 'Random update',
                           'builds': [{'nvr': 'foo-1.2-3.fc99',
                                       'package': {},
                                      }],
                           'other_keys': {},
                          }

    def test_query_existing_update(self):
        '''Test query for existing update'''
        ref_query_answer = {'tg_flash': None,
                            'num_items': 1,
                            'title': '1 update found',
                            'updates': [self.ref_update],
                           }
        stub_bodhi = Dingus(query__returns = ref_query_answer)
        bodhi = bodhi_utils.BodhiUtils(client=stub_bodhi)
        update = bodhi.query_update(self.ref_bodhi_id)

        assert update == self.ref_update

    def test_query_non_existing_update(self, monkeypatch):
        '''Test query for non-existing update'''
        ref_query_answer = {'tg_flash': None,
                            'num_items': 0,
                            'title': '0 updates found',
                            'updates': [],
                           }
        stub_bodhi = Dingus(query__returns = ref_query_answer)
        bodhi = bodhi_utils.BodhiUtils(client=stub_bodhi)
        update = bodhi.query_update(self.ref_bodhi_id)

        assert update is None

    def test_query_retry(self):
        '''Test whether query can retry if server returns HTTP 500'''
        ref_query_answer = create_failing_server({'tg_flash': None,
                                                  'num_items': 1,
                                                  'title': '1 update found',
                                                  'updates': [self.ref_update]})
        stub_bodhi = Dingus(query = ref_query_answer)
        bodhi = bodhi_utils.BodhiUtils(client=stub_bodhi)
        update = bodhi.query_update(self.ref_bodhi_id)

        assert update == self.ref_update

    def test_query_retry_fail(self):
        '''Test query on failing server'''
        stub_bodhi = Dingus(query = raise_500_server_error)
        bodhi = bodhi_utils.BodhiUtils(client=stub_bodhi)

        with pytest.raises(ServerError):
            bodhi.query_update(self.ref_bodhi_id)

        stub_bodhi = Dingus(query = raise_404_not_found)
        bodhi = bodhi_utils.BodhiUtils(client=stub_bodhi)

        with pytest.raises(ServerError):
            bodhi.query_update(self.ref_bodhi_id)


@pytest.mark.usefixtures('setup')
class TestBuild2Update():
    '''Test build2update()'''

    # fake requests and responses for query_update()
    requests = {
        'foo-ok-1.2-3.fc20': Bunch(builds=[Bunch(nvr=u'foo-ok-1.2-3.fc20')]),
        'foo-fail-1.2-3.fc20': None,
        'bar-ok-1.8.0-4.fc20':
            Bunch(builds=[Bunch(nvr=u'bar-ok-1.8.0-4.fc20')]),
        'bar-fail-1.8.0-4.fc20': None,
        'multi1-a-1.0-1.fc20':
            Bunch(builds=[Bunch(nvr=u'multi1-a-1.0-1.fc20'),
                          Bunch(nvr=u'multi1-b-2.0-2.fc20')]),
        'multi1-b-2.0-2.fc20':
            Bunch(builds=[Bunch(nvr=u'multi1-a-1.0-1.fc20'),
                          Bunch(nvr=u'multi1-b-2.0-2.fc20')]),
        'multi2-a-1.0-1.fc20':
            Bunch(builds=[Bunch(nvr=u'multi2-a-1.0-1.fc20'),
                          Bunch(nvr=u'multi2-b-2.0-2.fc20')]),
        'multi2-b-2.0-2.fc20':
            Bunch(builds=[Bunch(nvr=u'multi2-a-1.0-1.fc20'),
                          Bunch(nvr=u'multi2-b-2.0-2.fc20')]),
    }

    @pytest.fixture
    def setup(self, monkeypatch):
        '''Run this before every test invocation'''
        self.bodhi = bodhi_utils.BodhiUtils()
        # we shouldn't access the client anywhere, but just to be sure let's
        # erase it. At least we'll detect accidental access (an error thrown).
        self.bodhi.client = None
        # Replace query_update() with a fake function
        monkeypatch.setattr(self.bodhi, 'query_update',
                            lambda build: self.requests[build])

    def test_basic(self):
        '''One update per build, mixed results'''
        updates, failures = self.bodhi.build2update(['foo-ok-1.2-3.fc20',
                                                     'bar-fail-1.8.0-4.fc20'])

        assert len(updates) == 1
        assert 'foo-ok-1.2-3.fc20' in updates
        assert (updates['foo-ok-1.2-3.fc20'] is
                self.requests['foo-ok-1.2-3.fc20'])

        assert len(failures) == 1
        assert 'bar-fail-1.8.0-4.fc20' in failures
        assert (failures['bar-fail-1.8.0-4.fc20'] is
                self.requests['bar-fail-1.8.0-4.fc20'])

    def test_raise(self):
        '''Invalid input params'''
        # 'builds' not iterable of strings
        with pytest.raises(exc.TaskotronValueError):
            self.bodhi.build2update('foo-ok-1.2-3.fc20')

    def test_epoch(self):
        '''Should ignore epoch when asking about builds'''
        updates, failures = self.bodhi.build2update(['foo-ok-1:1.2-3.fc20',
                                                     'bar-ok-0:1.8.0-4.fc20'])

        assert not failures
        assert len(updates) == 2
        for update in updates.values():
            assert len(update['builds']) == 1
        assert (updates['foo-ok-1:1.2-3.fc20']['builds'][0]['nvr'] ==
                'foo-ok-1.2-3.fc20')
        assert (updates['bar-ok-0:1.8.0-4.fc20']['builds'][0]['nvr'] ==
                'bar-ok-1.8.0-4.fc20')

    def test_multi(self):
        '''Multiple builds in an update'''
        updates, failures = self.bodhi.build2update(['multi1-a-1.0-1.fc20',
                                                     'multi1-b-2.0-2.fc20',
                                                     'multi2-a-1.0-1.fc20',
                                                     'foo-fail-1.2-3.fc20'])

        assert len(updates) == 3
        assert 'multi1-a-1.0-1.fc20' in updates
        assert 'multi1-b-2.0-2.fc20' in updates
        assert (updates['multi1-a-1.0-1.fc20'] is updates['multi1-b-2.0-2.fc20']
                is self.requests['multi1-a-1.0-1.fc20'])
        # incomplete updates are allowed without strict=True
        assert 'multi2-a-1.0-1.fc20' in updates

        assert len(failures) == 1
        assert 'foo-fail-1.2-3.fc20' in failures
        assert (failures['foo-fail-1.2-3.fc20'] is
                self.requests['foo-fail-1.2-3.fc20'])

    def test_multi_strict(self):
        '''Multiple builds in an update with strict mode'''

        updates, failures = self.bodhi.build2update(['multi1-a-1.0-1.fc20',
                                                     'multi1-b-2.0-2.fc20',
                                                     'multi2-a-1.0-1.fc20',
                                                     'foo-fail-1.2-3.fc20'],
                                                     strict=True)

        assert len(updates) == 2
        assert 'multi1-a-1.0-1.fc20' in updates
        assert 'multi1-b-2.0-2.fc20' in updates
        assert (updates['multi1-a-1.0-1.fc20'] is updates['multi1-b-2.0-2.fc20']
                is self.requests['multi1-a-1.0-1.fc20'])

        assert len(failures) == 2
        assert 'multi2-a-1.0-1.fc20' in failures
        assert (failures['multi2-a-1.0-1.fc20'] is
                self.requests['multi2-a-1.0-1.fc20'])
        assert 'foo-fail-1.2-3.fc20' in failures
        assert (failures['foo-fail-1.2-3.fc20'] is
                self.requests['foo-fail-1.2-3.fc20'])
