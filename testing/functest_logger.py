# -*- coding: utf-8 -*-
# Copyright 2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

import os
import logging
import pytest

from libtaskotron import logger
from libtaskotron import config


class TestLogger():

    def setup_method(self, method):
        '''Run before every method'''
        # remember the list of root handlers
        self.root_handlers = logging.getLogger().handlers
        # remember the level of the stream handler
        self.stream_level = (logger.stream_handler.level if logger.stream_handler
                             else None)

    def teardown_method(self, method):
        '''Run after every method'''
        # reset the list of root handlers
        logging.getLogger().handlers = self.root_handlers
        # reset the stream handler level
        if logger.stream_handler:
            logger.stream_handler.level = self.stream_level


    def test_logfile(self, tmpdir):
        '''Messages should be logged to file when enabled'''
        log_path = tmpdir.join('test.log').strpath
        logger.init(filelog=True, filelog_path=log_path)

        msg = 'This should appear in the log file'
        logger.log.debug(msg)
        logger.file_handler.flush()

        with open(log_path) as log_file:
            lines = log_file.readlines()

        assert msg in lines[-1]

    def msg_in_tmp_log(self, msg, bad_log_path):
        '''A helper method to ensure a message has been logged to a temporary
        log file and not into (somehow inaccessible) requested log file. The
        temporary log file location is extracted from the config file.
        :param str msg: message to search for in a log file
        :param str bad_log_path: path to a requested log file which must not be
            possible to open
        :raise AssertError: if something is wrong
        '''
        # the temporary file must be somewhere else
        conf = config.get_config()
        tmp_log_path = os.path.join(conf.logdir, conf.log_name)
        assert tmp_log_path != bad_log_path

        # the inaccessible log file must not be possible to open
        with pytest.raises(IOError):
            open(bad_log_path)

        # the message must be in the temporary log file
        with open(tmp_log_path) as tmp_log:
            lines = tmp_log.readlines()
        assert msg in lines[-1]

    def test_logfile_no_write(self, tmpdir):
        '''If file log is not writeable, a temporary file should be used'''
        log_file = tmpdir.join('test.log')
        log_path = log_file.strpath
        log_file.write('')
        # make the file inaccessible for writing
        os.chmod(log_path, 0)

        logger.init(filelog=True, filelog_path=log_path)
        msg = 'This should be in a temporary log file'
        logger.log.debug(msg)
        logger.file_handler.flush()

        self.msg_in_tmp_log(msg, log_path)

    def test_logfile_missing_dir(self, tmpdir):
        '''If file log is not writeable, a temporary file should be used'''
        log_path = tmpdir.join('missing_dir', 'test.log').strpath

        logger.init(filelog=True, filelog_path=log_path)
        msg = 'This should be in a temporary log file'
        logger.log.debug(msg)
        logger.file_handler.flush()

        self.msg_in_tmp_log(msg, log_path)
