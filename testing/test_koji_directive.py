# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

from dingus import Dingus

from libtaskotron.directives import koji_directive

class TestKojiDirective():
    def setup_method(self, method):
        self.ref_nvr = 'foo-1.2-3.fc99'
        self.ref_arch = ['noarch']
        self.ref_tag = 'tagfoo'
        self.ref_name = 'foo'
        self.ref_version = '1.2'
        self.ref_release = '3.fc99'
        self.ref_buildid = 123456

        self.ref_build = {'package_name': self.ref_name,
                          'version': self.ref_version,
                          'release': self.ref_release,
                          'id': self.ref_buildid}

        self.ref_rpms = [{'name': self.ref_name, 'version': self.ref_version,
                          'release': self.ref_release, 'nvr': self.ref_nvr,
                          'arch': self.ref_arch},
                    {'name': self.ref_name, 'version': self.ref_version,
                     'release': self.ref_release, 'nvr': self.ref_nvr,
                     'arch': ['src']}]

        self.ref_tagged = [{'nvr': self.ref_nvr}]

        self.ref_url = 'http://downloads.example.com/builds/%s.noarch.rpm'\
                       % self.ref_nvr
        self.helper = koji_directive.KojiDirective()

        self.ref_envdata = {'workdir': '/var/tmp/foo'}

        self.ref_rpms = ['/var/tmp/fake/%s.%s.rpm'
                         % (self.ref_nvr, self.ref_arch)]

    def test_parse_download_command(self):

        self.ref_input = {'action': 'download',
                          'arch': self.ref_arch,
                          'koji_build': self.ref_nvr}

        stub_koji = Dingus(get_nvr_rpms__returns = self.ref_rpms)

        test_helper = koji_directive.KojiDirective(stub_koji)

        test_helper.process(self.ref_input, self.ref_envdata)

        getrpm_calls = stub_koji.calls()
        requested_nvr = getrpm_calls[0][1][0]
        requested_src = getrpm_calls[0][2]['src']

        assert len(getrpm_calls) == 1
        assert requested_nvr == self.ref_nvr
        assert not requested_src

    def test_parse_download_tag_command(self):

        self.ref_input = {'action': 'download_tag',
                          'arch': self.ref_arch,
                          'koji_tag': self.ref_tag}

        stub_koji = Dingus(get_tagged_rpms__returns=self.ref_rpms)

        test_helper = koji_directive.KojiDirective(stub_koji)

        test_helper.process(self.ref_input, self.ref_envdata)

        getrpm_calls = stub_koji.calls()
        requested_tag = getrpm_calls[0][1][0]

        assert len(getrpm_calls) == 1
        assert requested_tag == self.ref_tag

    def test_parse_multiple_arches(self):
        self.ref_arch = ['noarch', 'i386']

        self.ref_input = {'action': 'download_tag',
                          'arch': self.ref_arch,
                          'koji_tag': self.ref_tag}

        stub_koji = Dingus('koji_utils')

        test_helper = koji_directive.KojiDirective(stub_koji)
        test_helper.process(self.ref_input, self.ref_envdata)

        assert test_helper.arches == self.ref_arch

    def test_parse_single_arch(self):
        self.ref_input = {'action': 'download_tag',
                          'arch': self.ref_arch,
                          'koji_tag': self.ref_tag}

        stub_koji = Dingus('koji_utils')

        test_helper = koji_directive.KojiDirective(stub_koji)
        test_helper.process(self.ref_input, self.ref_envdata)

        assert test_helper.arches == self.ref_arch

    def test_add_noarch_download_command(self):
        ref_input = {'action': 'download', 'arch': ['x86_64'],
                     'koji_build': self.ref_nvr}
        ref_envdata = {'workdir': '/var/tmp/foo'}

        stub_koji = Dingus()
        test_helper = koji_directive.KojiDirective(stub_koji)
        test_helper.process(ref_input, ref_envdata)

        getrpm_calls = stub_koji.calls()
        requested_arches = getrpm_calls[0][2]['arches']

        assert len(getrpm_calls) == 1
        assert 'x86_64' in requested_arches
        assert 'noarch' in requested_arches

    def test_add_noarch_download_tag_command(self):
        ref_input = {'action': 'download_tag', 'arch': ['x86_64'],
                     'koji_tag': self.ref_tag}
        ref_envdata = {'workdir': '/var/tmp/foo'}

        stub_koji = Dingus(get_tagged_rpms__returns=self.ref_rpms)
        test_helper = koji_directive.KojiDirective(stub_koji)
        test_helper.process(ref_input, ref_envdata)

        getrpm_calls = stub_koji.calls()
        requested_arches = getrpm_calls[0][2]['arches']

        assert len(getrpm_calls) == 1
        assert 'x86_64' in requested_arches
        assert 'noarch' in requested_arches

    def test__download_command_src(self):
        ref_input = {'action': 'download', 'arch': [], 'src': True,
                     'koji_build': self.ref_nvr}
        ref_envdata = {'workdir': '/var/tmp/foo'}

        stub_koji = Dingus()
        test_helper = koji_directive.KojiDirective(stub_koji)
        test_helper.process(ref_input, ref_envdata)

        getrpm_calls = stub_koji.calls()
        requested_arches = getrpm_calls[0][2]['arches']
        requested_src = getrpm_calls[0][2]['src']

        assert len(getrpm_calls) == 1
        assert [] == requested_arches
        assert requested_src
