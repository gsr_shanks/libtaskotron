# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

import pytest
import os
import sys
import copy
from dingus import Dingus

from libtaskotron import runner
from libtaskotron import exceptions as exc


class TestRunnerInputVerify():
    def test_yamlrunner_valid_input(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build -a x86_64 footask.yml'
        ref_argdata = {'input': {'args': ['koji_build', 'arch']}}

        test_parser = runner.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = runner.process_args(test_input_args)

        test_runner = runner.Runner(ref_argdata, test_args)

        test_runner._validate_input()

    def test_yamlrunner_missing_input(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build footask.yml'
        ref_inputspec = {}

        test_parser = runner.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = runner.process_args(test_input_args)

        test_runner = runner.Runner(ref_inputspec, test_args)

        test_runner._validate_input()

    def test_yamlrunner_fails_empty_input(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build footask.yml'
        ref_inputspec = {'input': None}

        test_parser = runner.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = runner.process_args(test_input_args)

        test_runner = runner.Runner(ref_inputspec, test_args)

        with pytest.raises(exc.TaskotronYamlError):
            test_runner._validate_input()

    def test_yamlrunner_fails_empty_arg(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build footask.yml'
        ref_inputspec = {'input': {'args': None}}

        test_parser = runner.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = runner.process_args(test_input_args)

        test_runner = runner.Runner(ref_inputspec, test_args)

        with pytest.raises(exc.TaskotronYamlError):
            test_runner._validate_input()

    def test_yamlrunner_fails_missing_arg(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build footask.yml'
        ref_inputspec = {'input': 'other_item'}

        test_parser = runner.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = runner.process_args(test_input_args)

        test_runner = runner.Runner(ref_inputspec, test_args)

        with pytest.raises(exc.TaskotronYamlError):
            test_runner._validate_input()

    def test_yamlrunner_fails_undefined_arg(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build footask.yml'
        ref_inputspec = {'input': {'args': 'nonexistent_input_arg'}}

        test_parser = runner.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = runner.process_args(test_input_args)

        test_runner = runner.Runner(ref_inputspec, test_args)

        with pytest.raises(exc.TaskotronYamlError):
            test_runner._validate_input()

    def test_yamlrunner_fails_input_is_string(self):
        ref_cmd = '-i foo-1.2-3.fc99 -t koji_build footask.yml'
        ref_inputspec = {'input': {'args'}}

        test_parser = runner.get_argparser()
        test_input_args = vars(test_parser.parse_args(ref_cmd.split()))
        test_args = runner.process_args(test_input_args)

        test_runner = runner.Runner(ref_inputspec, test_args)

        with pytest.raises(exc.TaskotronYamlError):
            test_runner._validate_input()

class TestRunnerSetup():
    def test_trivial_creation(self, tmpdir):
        ref_taskdata = {'preparation': {'koji': 'download nvr'},
                    'input': {'args': ['koji_build','arch']},
                    'post': {'shell': 'clean'},
                    'execution': {'python': 'run_rpmlint.py'},
                    'dependencies': ['rpmlint', 'libtaskbot']}
        ref_inputdata = {}

        test_runner = runner.Runner(ref_taskdata, ref_inputdata, tmpdir)

        assert test_runner.taskdata == ref_taskdata

class TestRunnerDirectiveLoader():

    def setup_method(self, method):
        self.ref_dir = os.path.abspath('testing/test_directives/')
        self.ref_taskdata = {}
        self.ref_inputdata = {}
        self.ref_directivename = 'test'

    def test_runner_load_directive(self, tmpdir):
        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata,
                                    tmpdir)

        test_runner._load_directive(self.ref_directivename, self.ref_dir)

        assert '%s_directive' % self.ref_directivename in sys.modules.keys()

    def test_runner_load_directive_indict(self, tmpdir):
        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata,
                                    tmpdir)

        test_runner._load_directive(self.ref_directivename, self.ref_dir)

        assert self.ref_directivename in test_runner.directives.keys()

    def test_runner_load_directive_notexist(self, tmpdir):
        self.ref_directivename = 'noexist'

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata,
                                    tmpdir)

        with pytest.raises(exc.TaskotronYamlError):
            test_runner._load_directive(self.ref_directivename, self.ref_dir)

class TestRunnerSingleAction():

    def setup_method(self, method):
        self.ref_taskdata = {}
        self.ref_inputdata = {}

    def test_runner_extract_directive_name(self):
        ref_directivename = 'dummy'
        ref_action = {'name': 'test action',
                      ref_directivename: {'result': 'pass',
                                          'msg': 'dummy message'}}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        test_directivename = test_runner._extract_directive_from_action(
            ref_action)

        assert test_directivename == ref_directivename

    def test_runner_single_dummy_action_pass(self):
        ref_action = {'dummy': {'result': 'pass'}}
        ref_action.update({'name': 'Dummy Action' })

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        test_runner.do_single_action(ref_action)


    def test_runner_single_dummy_action_fail(self):
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'fail'}}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        with pytest.raises(exc.TaskotronDirectiveError):
            test_runner.do_single_action(ref_action)

    def test_runner_do_single_dummy_action_pass_export(self):
        ref_exportname = 'dummy_output'
        ref_output = 'this is example output'
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'pass', 'msg': ref_output},
                      'export': '%s' % ref_exportname}


        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        test_runner.do_single_action(ref_action)

        assert test_runner.working_data[ref_exportname] == ref_output

    def test_runner_do_single_dummy_action_pass_export_rendered_msg(self):
        ref_exportname = 'dummy_output'
        ref_message = 'This is a variable message'
        ref_messagename = 'variable_messsage'
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'pass',
                                'msg': '${%s}' % ref_messagename},
                      'export': '%s' % ref_exportname}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)
        test_runner.working_data[ref_messagename] = ref_message

        test_runner.do_single_action(ref_action)

        assert test_runner.working_data[ref_exportname] == ref_message


class TestRunnerDoActions():

    def setup_method(self, method):
        self.ref_taskdata = {}
        self.ref_inputdata = {}

    def test_runner_do_single_dummy_pass(self):
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'pass'}}
        self.ref_taskdata = {'actions': [ref_action]}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        test_runner.do_actions()

    def test_runner_do_single_dummy_fail(self):
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'fail'}}
        self.ref_taskdata = {'actions': [ref_action]}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        with pytest.raises(exc.TaskotronDirectiveError):
            test_runner.do_actions()

    def test_runner_do_multiple_dummy_fail(self):
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'fail'}}
        self.ref_taskdata = {'actions': [ref_action, ref_action]}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        with pytest.raises(exc.TaskotronDirectiveError):
            test_runner.do_actions()

    def test_runner_do_multiple_dummy_pass_export_rendered_msg(self):
        ref_exportname = 'dummy_output'
        ref_message = 'This is a variable message'
        ref_messagename = 'variable_messsage'
        ref_action = {'name': 'Dummy Action',
                      'dummy': {'result': 'pass',
                                'msg': '${%s}' % ref_messagename},
                      'export': '%s' % ref_exportname}

        self.ref_taskdata = {'actions': [ref_action, ref_action]}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)
        test_runner.working_data[ref_messagename] = ref_message

        test_runner.do_actions()

        assert test_runner.working_data[ref_exportname] == ref_message

    def test_runner_fails_missing_action(self):
        self.ref_taskdata = {'actions': None}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        with pytest.raises(exc.TaskotronYamlError):
            test_runner.do_actions()

    def test_runner_fails_missing_task(self):
        self.ref_taskdata = {}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)

        with pytest.raises(exc.TaskotronYamlError):
            test_runner.do_actions()


class TestRunnerRenderAction():

    def setup_method(self, method):
        self.ref_taskdata = {}
        self.ref_inputdata = {}

    def test_runner_render_single_variable(self):
        ref_message = 'This is a variable message'
        ref_messagename = 'variable_messsage'
        ref_action = {'name': 'test dummy action', 'dummy':
            {'result': 'pass', 'msg': '${%s}' % ref_messagename}}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)
        test_runner.working_data[ref_messagename] = ref_message

        test_rendered_action = test_runner._render_action(ref_action)

        assert test_rendered_action['dummy']['msg'] == ref_message

    def test_runner_render_list_variable(self):
        ref_var = ['foo', 'bar']
        ref_varname = 'testvar'
        ref_action = {'name': 'test dummy action', 'dummy':
            {'result': 'pass', 'msg': '${%s}' % ref_varname}}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)
        test_runner.working_data[ref_varname] = ref_var

        test_rendered_action = test_runner._render_action(ref_action)

        assert test_rendered_action['dummy']['msg'] == ref_var

    def test_runner_render_multi_variable(self):
        ref_var1 = 'foo'
        ref_var1name = 'testvar1'
        ref_var2 = 'bar'
        ref_var2name = 'testvar2'
        ref_action = {'name': 'test dummy action', 'dummy':
            {'result': 'pass', 'msg': '--foo ${%s} --bar ${%s}' %
            (ref_var1name, ref_var2name)}}

        test_runner = runner.Runner(self.ref_taskdata, self.ref_inputdata)
        test_runner.working_data[ref_var1name] = ref_var1
        test_runner.working_data[ref_var2name] = ref_var2

        test_rendered_action = test_runner._render_action(ref_action)

        assert (test_rendered_action['dummy']['msg'] == '--foo %s --bar %s' %
                (ref_var1, ref_var2))


class TestRunnerProcessArgs():

    def test_process_args_koji_build(self):
        ref_input = { 'arch': ['x86_64'],
                      'item': 'foo-1.2-3.fc99',
                      'type': 'koji_build',
                      'task': 'sometask.yml',
                      'override': []}

        ref_args = copy.deepcopy(ref_input)
        ref_args['koji_build'] = 'foo-1.2-3.fc99'

        test_args = runner.process_args(ref_input)

        assert test_args == ref_args

    def test_process_args_bodhi_id(self):
        ref_input = { 'arch': ['x86_64'],
                      'item': 'foo-1.2-3.fc99',
                      'type': 'bodhi_id',
                      'task': 'sometask.yml',
                      'override': []}

        ref_args = copy.deepcopy(ref_input)
        ref_args['bodhi_id'] = 'foo-1.2-3.fc99'

        test_args = runner.process_args(ref_input)

        assert test_args == ref_args

    def test_process_args_koji_tag(self):
        ref_input = { 'arch': ['x86_64'],
                      'item': 'dist-fc99-updates',
                      'type': 'koji_tag',
                      'task': 'sometask.yml',
                      'override': []}

        ref_args = copy.deepcopy(ref_input)
        ref_args['koji_tag'] = 'dist-fc99-updates'

        test_args = runner.process_args(ref_input)

        assert test_args == ref_args

    def test_process_args_koji_tag_multiple_arches(self):
        ref_input = { 'arch': ['x86_64', 'i386', 'noarch'],
                      'item': 'dist-fc99-updates',
                      'type': 'koji_tag',
                      'task': 'sometask.yml',
                      'override': []}

        ref_args = copy.deepcopy(ref_input)
        ref_args['koji_tag'] = 'dist-fc99-updates'

        test_args = runner.process_args(ref_input)

        assert test_args == ref_args

    def test_process_args_no_arch(self):
        ref_input = { 'type': 'invalid',
                      'arch': None,
                      'override': []}
        ref_args = copy.deepcopy(ref_input)
        ref_args['arch'] = ['noarch']

        test_args = runner.process_args(ref_input)
        assert test_args == ref_args


class TestRunnerCheckOverride():

    def test_override_existing(self):
        ref_input = { 'arch': ['x86_64'],
                      'type': 'koji_build',
                      'item': 'foo-1.2-3.fc99',
                      'override': ["workdir='/some/dir/'"]}

        ref_inputdata = runner.process_args(ref_input)

        test_runner = runner.Runner(Dingus(), ref_inputdata)
        test_runner.do_actions = lambda: None
        test_runner.run()

        assert test_runner.envdata['workdir'] == "/some/dir/"

    def test_override_nonexisting(self):
        ref_input = { 'arch': ['x86_64'],
                      'type': 'koji_build',
                      'item': 'foo-1.2-3.fc99',
                      'override': ["friendship='is magic'"]}

        ref_inputdata = runner.process_args(ref_input)

        test_runner = runner.Runner(Dingus(), ref_inputdata)
        test_runner.do_actions = lambda: None
        test_runner.run()

        assert test_runner.envdata['friendship'] == "is magic"
