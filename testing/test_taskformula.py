# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Unit tests for libtaskotron/taskformula.py'''

import copy

import yaml
import pytest
from libtaskotron import taskformula
import libtaskotron.exceptions as exc


ref_taskname = 'testtask'
ref_taskdesc = 'this is a test task'
ref_maintainer = 'testuser'
ref_deps = ['libtaskotron', 'libpanacea']
ref_input = {'args': 'arch, koji_build'}
ref_actions = []


def create_yamldata(name=ref_taskname, desc=ref_taskdesc, maint=ref_maintainer,
                    deps=ref_deps, input_=ref_input, actions=ref_actions):
    return {'name': name,
            'desc': desc,
            'maintainer': maint,
            'deps': deps,
            'input': input_,
            'actions': actions}


class TestTaskYamlMetadata(object):
    def setup_method(self, method):
        self.taskdata = create_yamldata()
        self.test_task = taskformula.parse_yaml(yaml.safe_dump(self.taskdata))

    def test_taskyaml_taskname(self):
        assert self.test_task['name'] == self.taskdata['name']

    def test_taskyaml_taskdesc(self):
        assert self.test_task['desc'] == self.taskdata['desc']

    def test_taskyaml_taskmaintainer(self):
        assert self.test_task['maintainer'] == self.taskdata['maintainer']

    def test_taskyaml_taskdeps(self):
        assert self.test_task['deps'] == self.taskdata['deps']

    def test_taskyaml_taskinput(self):
        assert self.test_task['input'] == self.taskdata['input']


class TestTaskYamlActionsNoVariables(object):
    def test_taskyaml_simple_action(self):
        ref_action = {'name': 'testaction', 'action': {'key': 'value'}}
        taskdata = create_yamldata(actions=[ref_action])

        test_task = taskformula.parse_yaml(yaml.safe_dump(taskdata))

        assert test_task['actions'][0] == ref_action

    def test_taskyaml_multiple_actions(self):
        ref_action = {'name': 'testaction', 'action': {'key': 'value'}}
        taskdata = create_yamldata(actions=[ref_action, ref_action, ref_action])

        test_task = taskformula.parse_yaml(yaml.safe_dump(taskdata))

        assert len(test_task['actions']) == 3


class TestReplaceVarsInAction(object):

    def setup_method(self, method):
        self.action = {
            'name': 'run foo${foo}',
            'export': '${export}',
            'python': {
                'file': 'somefile.py',
                'args': ['arg1', '${number}', 'arg2', '${list}', '${dict}'],
                'cmdline': '-s ${foo} -i ${number}',
                'nest': {
                    'deepnest': '${dict}',
                    'null': '${null}',
                }
            }
        }
        self.vars = {
            'foo': 'bar',
            'export': 'a:b\nc:d\n-e',
            'number': 1,
            'list': [1, 'item'],
            'dict': {'key': 'val', 'pi': 3.14},
            'null': None,
            'extra': 'juicy',
        }

    def test_complex(self):
        rend_action = copy.deepcopy(self.action)
        rend_action['name'] = 'run foo%s' % self.vars['foo']
        rend_action['export'] = self.vars['export']
        rend_action['python']['args'] = ['arg1', self.vars['number'], 'arg2',
                                         self.vars['list'], self.vars['dict']]
        rend_action['python']['cmdline'] = '-s %s -i %s' % (self.vars['foo'],
                                                            self.vars['number'])
        rend_action['python']['nest']['deepnest'] = self.vars['dict']
        rend_action['python']['nest']['null'] = self.vars['null']

        taskformula.replace_vars_in_action(self.action, self.vars)
        assert self.action == rend_action

    def test_raise(self):
        with pytest.raises(exc.TaskotronYamlError):
            taskformula.replace_vars_in_action(self.action, {})

        del self.vars['number']
        with pytest.raises(exc.TaskotronYamlError):
            taskformula.replace_vars_in_action(self.action, self.vars)


class TestReplaceVars(object):

    def test_standadlone_var_str(self):
        res = taskformula._replace_vars('${foo}', {'foo': 'hedgehog'})
        assert res == 'hedgehog'

    def test_standalone_var_nonstr(self):
        res = taskformula._replace_vars('${foo}', {'foo': ['chicken', 'egg']})
        assert res == ['chicken', 'egg']

    def test_standadlone_var_str_alternative(self):
        res = taskformula._replace_vars('$foo', {'foo': 'hedgehog'})
        assert res == 'hedgehog'

    def test_standalone_var_nonstr_alternative(self):
        res = taskformula._replace_vars('$foo', {'foo': ['chicken', 'egg']})
        assert res == ['chicken', 'egg']

    def test_multivar(self):
        res = taskformula._replace_vars('a ${b} c${d}', {'b': 'bb', 'd': 'dd'})
        assert res == 'a bb cdd'

    def test_empty_space(self):
        '''Some empty space around a single variable is like multi-var, i.e.
        a string is always returned'''
        res = taskformula._replace_vars(' ${foo}', {'foo': 'bar'})
        assert res == ' bar'

        res = taskformula._replace_vars(' ${foo}', {'foo': 42})
        assert res == ' 42'

    def test_recursive_content(self):
        '''The content of a variable contains more variable names. But those
        must not be expanded.'''
        text = '${foo} ${bar}'
        varz = {'foo': '${foo} ${bar}',
                'bar': '${foo} ${bar} ${baz}'
               }
        res = taskformula._replace_vars(text, varz)
        assert res == '%s %s' % (varz['foo'], varz['bar'])

    def test_empty(self):
        res = taskformula._replace_vars('', {'extra': 'var'})
        assert res == ''

    def test_same_var(self):
        '''Only variables must get replaced, not same-sounding text'''
        res = taskformula._replace_vars('foo ${foo}', {'foo': 'bar'})
        assert res == 'foo bar'

    def test_combined_syntax(self):
        '''Both $foo and ${foo} syntax must work, together with escaping'''
        res = taskformula._replace_vars('foo $foo ${foo} $$foo', {'foo': 'bar'})
        assert res == 'foo bar bar $foo'

    def test_raise_missing_var(self):
        with pytest.raises(exc.TaskotronYamlError):
            taskformula._replace_vars('${foo}', {'bar': 'foo'})

    def test_raise_missing_vars(self):
        with pytest.raises(exc.TaskotronYamlError):
            taskformula._replace_vars('${foo} ${bar}', {'bar': 'foo'})

    def test_raise_bad_escape(self):
        '''Dollars must be doubled'''
        with pytest.raises(exc.TaskotronYamlError):
            taskformula._replace_vars('${foo} $', {'foo': 'bar'})

