# -*- coding: utf-8 -*-
# Copyright 2009-2014, Red Hat, Inc.
# License: GPL-2.0+ <http://spdx.org/licenses/GPL-2.0+>
# See the LICENSE file for more details on Licensing

'''Unit tests for libtaskotron/koji_utils.py'''

import pytest
from dingus import Dingus

from libtaskotron import koji_utils
from libtaskotron import exceptions as exc
from libtaskotron import config


# http://stackoverflow.com/questions/3190706/nonlocal-keyword-in-python-2-x
def create_multicall(first, second):
    first_call = {"value": True}

    def multicall():
        if first_call["value"]:
            first_call["value"] = False
            return first
        else:
            return second

    return multicall


class TestKojiClient():
    def setup_method(self, method):
        self.ref_nvr = 'foo-1.2-3.fc99'
        self.ref_arch = 'noarch'
        self.ref_name = 'foo'
        self.ref_version = '1.2'
        self.ref_release = '3.fc99'
        self.ref_buildid = 123456
        self.ref_filename = "%s.%s.rpm" % (self.ref_nvr, self.ref_arch)

        self.ref_build = {'package_name': self.ref_name,
                          'version': self.ref_version,
                          'release': self.ref_release,
                          'id': self.ref_buildid}

        self.ref_rpms = [{'name': self.ref_name, 'version': self.ref_version,
                          'release': self.ref_release, 'nvr': self.ref_nvr,
                          'arch': self.ref_arch, 'build_id': self.ref_buildid,},
                    {'name': self.ref_name, 'version': self.ref_version,
                     'release': self.ref_release, 'nvr': self.ref_nvr,
                     'arch': 'src', 'build_id': self.ref_buildid, },
                    {'name': self.ref_name + '-debuginfo',
                     'version': self.ref_version,
                     'release': self.ref_release, 'nvr': self.ref_nvr,
                     'arch': 'src', 'build_id': self.ref_buildid, },
                    {'name': self.ref_name + '-debuginfo-common',
                     'version': self.ref_version,
                     'release': self.ref_release, 'nvr': self.ref_nvr,
                     'arch': 'src', 'build_id': self.ref_buildid, }]

    def test_rpms_to_build(self):
        stub_koji = Dingus(getBuild__returns=None,
                           getRPM__returns=None,
                           multiCall=create_multicall(
                               [[self.ref_rpms[0]], [self.ref_rpms[1]]],
                               [[self.ref_build], [self.ref_build]]))

        test_koji = koji_utils.KojiClient(stub_koji)
        outcome = test_koji.rpms_to_build([self.ref_filename,self.ref_filename])

        assert outcome == [self.ref_build, self.ref_build]
        # because two rpms come from same build, it gets called twice for each
        # rpm, once for build
        assert len(stub_koji.calls) == 3


    def test_rpms_to_build_exceptions(self):
        stub_koji = Dingus(getRPM__returns=None,
                           multiCall__returns=[{"faultCode": -1,
                                                "faultString": "failed"}])
        test_koji = koji_utils.KojiClient(stub_koji)

        with pytest.raises(exc.TaskotronRemoteError):
            test_koji.rpms_to_build([self.ref_filename])

        stub_koji = Dingus(getBuild__returns=None,
                           getRPM__returns=None,
                           multiCall=create_multicall([[self.ref_rpms[0]]], [
                               {"faultCode": -1, "faultString": "failed"}]))

        test_koji = koji_utils.KojiClient(stub_koji)
        with pytest.raises(exc.TaskotronRemoteError):
            test_koji.rpms_to_build([self.ref_filename])

        stub_koji = Dingus(getBuild__returns=None,
                           getRPM__returns=None,
                           multiCall__returns=[[None]])

        test_koji = koji_utils.KojiClient(stub_koji)
        with pytest.raises(exc.TaskotronRemoteError):
            test_koji.rpms_to_build([self.ref_filename])

        stub_koji = Dingus(getBuild__returns=None,
                           getRPM__returns=None,
                           multiCall=create_multicall([[self.ref_rpms[0]]],
                                                      [[None]]))

        test_koji = koji_utils.KojiClient(stub_koji)
        with pytest.raises(exc.TaskotronRemoteError):
            test_koji.rpms_to_build([self.ref_filename])


    def test_get_noarch_rpmurls_from_nvr(self):
        stub_koji = Dingus(getBuild__returns = self.ref_build,
                           listRPMs__returns = self.ref_rpms)

        test_koji = koji_utils.KojiClient(stub_koji)

        test_urls = test_koji.nvr_to_urls(self.ref_nvr)

        koji_baseurl = config.get_config().pkg_url
        ref_urls = ['%s/%s/%s/%s/%s/%s.%s.rpm' % (koji_baseurl, self.ref_name,
                        self.ref_version, self.ref_release, self.ref_arch,
                        self.ref_nvr, self.ref_arch),
                    '%s/%s/%s/%s/src/%s.src.rpm' % (koji_baseurl, self.ref_name,
                        self.ref_version, self.ref_release, self.ref_nvr),
                    ]

        assert test_urls == ref_urls

    def should_i386_rpmurls_query_i686(self):
        self.ref_arch = 'i386'

        stub_koji = Dingus(getBuild__returns = self.ref_build)

        test_koji = koji_utils.KojiClient(stub_koji)

        # this isn't setup to actually return urls since we don't care about
        # that for this test
        # it should throw an exception in this case.
        try:
            test_koji.nvr_to_urls(self.ref_nvr, arches = [self.ref_arch])
        except exc.TaskotronRemoteError:
            pass

        listrpm_calls = stub_koji.calls('listRPMs')
        requested_arches = listrpm_calls[0][2]['arches']

        assert 'i686' in requested_arches
        assert 'i386' in requested_arches

    def should_not_throw_exception_norpms(self):
        '''It's possible to have no RPMs (for the given arch) in a build'''
        stub_koji = Dingus(getBuild__returns = self.ref_build)
        test_koji = koji_utils.KojiClient(stub_koji)

        test_koji.nvr_to_urls(self.ref_nvr, arches = [self.ref_arch])

    def test_nvr_to_urls_src_filtering(self):
        stub_koji = Dingus(getBuild__returns=self.ref_build,
                           listRPMs__returns=self.ref_rpms)
        test_koji = koji_utils.KojiClient(stub_koji)

        # arch and src enabled
        urls = test_koji.nvr_to_urls(self.ref_nvr, arches=[self.ref_arch])
        assert len(urls) == 2
        assert 'src' in urls[0] or 'src' in urls[1]

        # arch enabled, src disabled
        urls = test_koji.nvr_to_urls(self.ref_nvr, arches=[self.ref_arch],
                                     src=False)
        assert len(urls) == 1
        assert 'src' not in urls[0]

    def test_nvr_to_urls_debuginfo(self):
        stub_koji = Dingus(getBuild__returns=self.ref_build,
                           listRPMs__returns=self.ref_rpms)
        test_koji = koji_utils.KojiClient(stub_koji)

        # arch enabled, src enabled, debuginfo enabled
        urls = test_koji.nvr_to_urls(self.ref_nvr, arches=[self.ref_arch],
                                     debuginfo=True)
        assert len(urls) == 4
        assert len([url for url in urls if '-debuginfo' in url]) == 2

    def test_nvr_to_urls_only_src(self):
        stub_koji = Dingus(getBuild__returns=self.ref_build)
        test_koji = koji_utils.KojiClient(stub_koji)

        # arch disabled, src enabled
        test_koji.nvr_to_urls(self.ref_nvr, arches=[], src=True)
        listrpm_calls = stub_koji.calls('listRPMs')
        requested_arches = listrpm_calls[0][2]['arches']
        assert len(requested_arches) == 1
        assert requested_arches[0] == 'src'

    def should_raise_no_arch_no_src(self):
        stub_koji = Dingus()
        test_koji = koji_utils.KojiClient(stub_koji)

        with pytest.raises(exc.TaskotronValueError):
            test_koji.nvr_to_urls(self.ref_nvr, arches=[], src=False)


class TestGetENVR(object):

    def test_epoch(self):
        '''Epoch included in build'''
        build = {'nvr': 'foo-1.2-3.fc20',
                 'epoch': 1,
                }
        assert koji_utils.getNEVR(build) == 'foo-1:1.2-3.fc20'

    def test_no_epoch(self):
        '''Epoch not included in build'''
        build = {'nvr': 'foo-1.2-3.fc20',
                 'epoch': None,
                }
        assert koji_utils.getNEVR(build) == 'foo-1.2-3.fc20'

    def test_raise(self):
        '''Invalid input param'''
        with pytest.raises(exc.TaskotronValueError):
            koji_utils.getNEVR('foo-1.2-3.fc20')
