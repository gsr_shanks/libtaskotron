# sitelib for noarch packages, sitearch for others (remove the unneeded one)
%{!?python_sitelib: %global python_sitelib %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib())")}
%{!?python_sitearch: %global python_sitearch %(%{__python} -c "from distutils.sysconfig import get_python_lib; print(get_python_lib(1))")}

Name:           libtaskotron
Version:        0.3.10
Release:        1%{?dist}
Summary:        Taskotron Support Library

License:        GPLv3
URL:            https://bitbucket.org/fedoraqa/libtaskotron
Source0:        http://qadevel.fedoraproject.org/releases/%{name}/%{name}-%{version}.tar.gz

BuildArch:      noarch

Requires:       bodhi-client
Requires:       koji
Requires:       rpm-python
Requires:       pyOpenSSL
Requires:       python-pycurl
Requires:       python-urlgrabber
Requires:       pytap13 >= 0.1.0
Requires:       resultsdb_api
Requires:       python-bayeux
Requires:       resultsdb_api
Requires:       python-hawkey >= 0.4.13-1
Requires:       python-bunch
Requires:       python-fedora
Requires:       createrepo
Requires:       mash
Requires:       libtaskotron-config
BuildRequires:  python-devel
BuildRequires:  python-setuptools
BuildRequires:  pytest
BuildRequires:  python-bunch
BuildRequires:  python-dingus
BuildRequires:  python-urlgrabber
BuildRequires:  koji
BuildRequires:  pytap13 >= 0.3.0
BuildRequires:  python-bayeux
BuildRequires:  bodhi-client
BuildRequires:  resultsdb_api
BuildRequires:  python-hawkey >= 0.4.13-1
BuildRequires:  python-fedora
BuildRequires:  mash


%description
Libtaskotron is a support library for running taskotron tasks.

%package -n libtaskotron-config
Summary:        Configuration files needed for using libtaskotron

%description -n libtaskotron-config
libtaskotron-config contains all of the configuration files needed for using
libtaskotron.

%prep
%setup -q

%check
%{__python} setup.py test

%build
%{__python} setup.py build

%install
rm -rf $RPM_BUILD_ROOT
%{__python} setup.py install -O1 --skip-build --root %{buildroot}

# configuration files
mkdir -p %{buildroot}%{_sysconfdir}/taskotron/
install conf/taskotron.yaml.example %{buildroot}%{_sysconfdir}/taskotron/taskotron.yaml
install conf/yumrepoinfo.conf.example %{buildroot}%{_sysconfdir}/taskotron/yumrepoinfo.conf

# log dir
install -m 777 -d %{buildroot}/%{_localstatedir}/log/taskotron


%files
%doc readme.rst LICENSE
%{python_sitelib}/libtaskotron
%{python_sitelib}/*.egg-info

%attr(755,root,root) %{_bindir}/runtask

%dir %attr(777, root, root) %{_localstatedir}/log/taskotron

%files -n libtaskotron-config
%dir %{_sysconfdir}/taskotron
%config(noreplace) %{_sysconfdir}/taskotron/taskotron.yaml
%config %{_sysconfdir}/taskotron/yumrepoinfo.conf



%changelog
* Wed Oct 22 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.10-1
- Fix for i386 unit tests (T361)
- Small documentation fixes

* Fri Oct 17 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.9-1
- Improve logging messages
- Update documentation

* Thu Oct 9 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.8-1
- Adding bodhi query retries for read-only operations (T338)
- several small bugfixes and typo corrections

* Fri Aug 22 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.7-1
- Adding mash as a BR for functional tests
- removing all as an option for runtask

* Fri Aug 22 2014 Martin Krizek <mkrizek@redhat.com> - 0.3.6-1
- Releasing libtaskotron 0.3.6

* Tue Jul 08 2014 Martin Krizek <mkrizek@fedoraproject.org> - 0.3.3-2
- Add /var/log/taskotron directory

* Mon Jun 30 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.3-1
- Changed distibution license to gpl3
- New user-facing docs

* Mon Jun 23 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.2-1
- Gracefully handle missing rpms in build. Fixes T251.

* Mon Jun 23 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.1-1
- Better support for depcheck in koji_utils and mash_directive
- Added ability for check name to be specified in TAP13 output

* Mon Jun 16 2014 Tim Flink <tflink@fedoraproject.org> - 0.3.0-1
- Added sphinx to requirements.txt

* Fri Jun 13 2014 Tim Flink <tflink@fedoraproject.org> - 0.2.1-1
- documentation improvements, added LICENSE file
- better support for depcheck, srpm downloading
- improved logging configuration

* Wed May 28 2014 Tim Flink <tflink@fedoraproject.org> - 0.1.1-1
- adding libtaskotron-config as requires for libtaskotron
- changing variable syntax to $var and ${var}
- add yumrepoinfo directive, other bugfixes

* Fri May 16 2014 Tim Flink <tflink@fedoraproject.org> - 0.1.0-1
- Releasing libtaskotron 0.1

* Fri May 09 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.11-4
- Disabling %check so that the code can be used while the tests are fixed

* Tue May 06 2014 Kamil Páral <kparal@redhat.com> - 0.0.11-3
- Add a minimum version to python-hawkey. Older versions have a different API.

* Tue Apr 29 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.11-2
- moved config files to libtaskotron-config subpackage

* Tue Apr 29 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.11-1
- changed config files to be noreplace
- updated config file source paths

* Tue Apr 29 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.10-1
- yumrepo fix for upgradepath, output fix for readability

* Fri Apr 25 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.9-1
- koji_utils fix for upgradepath

* Fri Apr 25 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.8-1
- Updating to latest upstream version
- Fixing resultsdb integration, adding some rpm and koji utility methods

* Wed Apr 16 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.7-2
- Fixing some urls and other small packaging changes

* Tue Apr 15 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.7-1
- Updating to latest upstream
- Change to more generic CLI arguments to match reporting and work better with buildbot

* Mon Apr 14 2014 Tim Flink <tflink@fedoraproject.org> - 0.0.6-2
- Initial package for libtaskotron
