=====================
libtaskotron API docs
=====================

This isn't final API documentation, more of an example of how we can do API docs.

Once more of our code is using sphinx-style docs, we can start finalizing the
format for this page.

Note that the docs here are for libtaskotron library functions and don't
include directives or task yaml details.


bodhi_utils
===========

.. automodule:: libtaskotron.bodhi_utils
   :members:

check
=====

.. automodule:: libtaskotron.check
   :members:

config
======

.. automodule:: libtaskotron.config
   :members:

config_defaults
===============

.. automodule:: libtaskotron.config_defaults
   :members:

exceptions
==========

.. automodule:: libtaskotron.exceptions
   :members:

file_utils
==========

.. automodule:: libtaskotron.file_utils
   :members:

koji_utils
==========

.. automodule:: libtaskotron.koji_utils
   :members:

logger
======

.. automodule:: libtaskotron.logger
   :members:

python_utils
============

.. automodule:: libtaskotron.python_utils
   :members:

rpm_utils
=========

.. automodule:: libtaskotron.rpm_utils
   :members:

runner
======

.. automodule:: libtaskotron.runner
   :members:

yumrepoinfo
===========

.. automodule:: libtaskotron.yumrepoinfo
   :members:
