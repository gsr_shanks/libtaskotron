.. _tap13-format:

================
TAP in Taskotron
================

The required output format of the checks in Taskotron is the `Test Anything Protocol (TAP) v13
<http://podwiki.hexten.net/TAP/TAP13.html?page=TAP13>`_. 

We recommend using the provided :py:class:`libtaskotron.check.CheckDetail` and the
helper method :py:meth:`libtaskotron.check.export_TAP` to create the TAP output.
If you can not use it, then please adhere to the TAP13 specification, and add
the required additional information (described below) to the YAML block.


TAP and reporting
=================

First up, we will provide you with two examples of the valid TAP output - the minimal
version, which just covers requirements, and then the extended version, which uses the
TAP format (and additional data in the YAML block) to its full potential Taskotron-vise.


Minimal version
---------------

If you don't need anything fancy, then this is the minimalistic version of the TAP output
which, at the same time, is valid for results reporting in Taskotron::

  TAP version 13
  1..1
  ok
    ---
    item: tzdata-2014f-1.fc20
    type: bodhi_update
    ...

Apart of the standard TAP format, you just need to provide the ``item`` and ``type`` values in
the YAML block.

The TAP state ``ok`` is interpreted as ``PASSED``, the ``not ok`` as ``FAILED``.

``type`` describes "what" was on a high level - whether it was a build from Koji, DVD image,
update in Bodhi, or something completely different. At the moment, we have internally
standardized three ``type`` values for the most common items we test:

  * ``koji_build`` for testing new builds in Koji
  * ``bodhi_update`` to represent results tied to a specific Bodhi update
  * ``yum_repository`` for repo-sanity checks, and so on

You can, of course, use any other value for the ``type`` parameter, but if you have a check
for repositories, Koji builds or Bodhi updates, please stick to the already defined ones,
so the tools next in the chain will recognize your results properly.

``item`` is a string representation of the specific piece, that was tested - NVR for the ``koji_build``
type, update-name or update-id for the ``bodhi_update``, etc. Once again - you can set whatever
value you think is right, but sticking to the reasonable/obvious choices will ensure seamless
processing.

.. Note::

  ``item``, and ``type`` are just some of the "reserved" key-names in the YAML block. Refer to the
  next section for the full list.


Full version
------------

Although the minimal version of the TAP output is sufficient, you can use additional fields
to provide more information::

  TAP version 13
  1..1
  not ok
    ---
    item: tzdata-2014f-1.fc20
    type: bodhi_update
    outcome: ABORTED
    summary: mytask ABORTED for tzdata-2014f-1.fc20
    details:
      output: |-
        tzdata-2014f-1.fc20.x86_64.rpm: ABORTED
        tzdata-java-2014f-1.fc20.x86_64.rpm: PASSED
    arch: x86_64
    package: tzdata
    ...

Going top-down, the new fields are ``outcome``, ``summary``, and ``details/output``.

The ``outcome`` field overrides the basic TAP state (``ok``/``not ok``). Values allowed at the moment, are
``PASSED``, ``INFO``, ``FAILED``, ``ERROR``, ``WAIVED``, and ``NEEDS_INSPECTION``. Using other values will cause
the ResultsDB reporting to fail.

``summary`` field allows you to add a comprehensive brief text describing the check's outcome.

The ``output`` key in the ``details`` is usefull for cherry-picking the important sections of your
check's output. Although it is not stored in the ResultsDB, it allows the people reading logs, to find
the important information in an easy fashion, without the need to go through the whole of it.

Note on the reserved field names
--------------------------------

``item``, ``type``, ``outcome``, ``summary``, ``details`` are the only "reserved" fields. We also decided to use
``arch`` as a field with "recommended meaning" (to represent the architecture). You can then add any
number of custom key-value pairs (like the ``package`` in the above example), which will get stored
in ResultsDB, and can be then used for searching.
