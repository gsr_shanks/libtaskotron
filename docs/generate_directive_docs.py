#!/usr/bin/env python
# (c) 2012, Jan-Piet Mens <jpmens () gmail.com>
#
# This file is part of Ansible
#
# Ansible is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# Ansible is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with Ansible.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import glob
import sys
import yaml
import ast
import re
import optparse
import datetime
import cgi
import traceback

from jinja2 import Environment, FileSystemLoader

from docutils.core import publish_parts

# constants and paths

# if a module is added in a version of Taskotron older than this, don't print
# the version added information in the module documentation because everyone
# is assumed to be running something newer than this already.
TO_OLD_TO_BE_NOTABLE = 0.0

# Get parent directory of the directory this script lives in
MODULEDIR = os.path.abspath(os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    os.pardir,
    'libtaskotron'))

_ITALIC = re.compile(r"I\(([^)]+)\)")
_BOLD = re.compile(r"B\(([^)]+)\)")
_MODULE = re.compile(r"M\(([^)]+)\)")
_URL = re.compile(r"U\(([^)]+)\)")
_CONST = re.compile(r"C\(([^)]+)\)")

# modules that are ok that they do not have documentation strings
BLACKLIST_MODULES = [
    '__init__.py',
]


def rst_ify(text):
    ''' convert symbols like I(this is in italics) to valid restructured
    text '''

    t = _ITALIC.sub(r'*' + r"\1" + r"*", text)
    t = _BOLD.sub(r'**' + r"\1" + r"**", t)
    t = _MODULE.sub(r'``' + r"\1" + r"``", t)
    t = _URL.sub(r"\1", t)
    t = _CONST.sub(r'``' + r"\1" + r"``", t)

    return t


def html_ify(text):
    ''' convert rst to html '''

    out = publish_parts(str(text), writer_name="html")['body']
    out = out.strip()
    return out


def nl2br(value):
    ''' convert line feeds to <br/> '''

    return value.replace('\n', '<br/>\n')


def rst_fmt(text, fmt):
    ''' helper for Jinja2 to do format strings '''

    return fmt % (text)


def rst_xline(width, char="="):
    ''' return a restructured text line of a given length '''

    return char * width


def write_data(text, options, outputname, module):
    ''' dumps module output to a file or the screen, as requested '''

    if options.output_dir is not None:
        # Hack the opening of the file to strip ".py" from module names
        f = open(os.path.join(options.output_dir,
                              outputname % module[:-3]), 'w')
        f.write(text.encode('utf-8'))
        f.close()
    else:
        print text


def list_modules(module_dir):
    ''' returns a hash of categories, each category being a hash of module
    names to file paths '''

    categories = dict(all=dict())
    files = glob.glob("%s/*" % module_dir)
    for d in files:
        if os.path.isdir(d):
            files2 = glob.glob("%s/*" % d)

            for f in files2:
                if not os.path.isdir(f):
                    tokens = f.split("/")
                    module = tokens[-1]
                    category = tokens[-2]
                    if category not in categories:
                        categories[category] = {}
                    categories[category][module] = f
                    categories['all'][module] = f
        else:
            # There is some ugly repitition here. Will remove later.
            tokens = d.split("/")
            module = tokens[-1]
            category = tokens[-2]
            if category not in categories:
                categories[category] = {}
            categories[category][module] = d
            categories['all'][module] = d

    return categories


def generate_parser():
    ''' generate an optparse parser '''

    p = optparse.OptionParser(
        version='%prog 1.0',
        usage='usage: %prog [options] arg1 arg2',
        description='Generate module documentation from metadata',
    )

    p.add_option("-A",
                 "--ansible-version",
                 action="store",
                 dest="ansible_version",
                 default="unknown",
                 help="Taskotron version number")

    p.add_option("-M",
                 "--module-dir",
                 action="store",
                 dest="module_dir",
                 default=MODULEDIR,
                 help="Taskotron library path")

    p.add_option("-T",
                 "--template-dir",
                 action="store",
                 dest="template_dir",
                 default="hacking/templates",
                 help="directory containing Jinja2 templates")

    p.add_option("-t",
                 "--type",
                 action='store',
                 dest='type',
                 choices=['rst'],
                 default='rst',
                 help="Document type")

    p.add_option("-v",
                 "--verbose",
                 action='store_true',
                 default=False,
                 help="Verbose")

    p.add_option("-o",
                 "--output-dir",
                 action="store",
                 dest="output_dir",
                 default=None,
                 help="Output directory for module files")

    p.add_option("-I",
                 "--includes-file",
                 action="store",
                 dest="includes_file",
                 default=None,
                 help="Create a file containing list of processed modules")

    p.add_option('-V',
                 action='version',
                 help='Show version number and exit')

    return p


def jinja2_environment(template_dir, typ):
    ''' Setup the jinja2 env to render our template '''

    env = Environment(loader=FileSystemLoader(template_dir),
                      variable_start_string="@{",
                      variable_end_string="}@",
                      trim_blocks=True)
    env.globals['xline'] = rst_xline

    if typ == 'rst':
        env.filters['convert_symbols_to_format'] = rst_ify
        env.filters['html_ify'] = html_ify
        env.filters['fmt'] = rst_fmt
        env.filters['xline'] = rst_xline
        env.filters['nl2br'] = nl2br
        template = env.get_template('rst.j2')
        outputname = "%s_module.rst"
    else:
        raise Exception("unknown module format type: %s" % typ)

    return env, template, outputname


def process_module(module, options, env, template, outputname, module_map):
    ''' Extract the DOCUMENTATION and EXAMPLES strings from a module,
    templatize them and write the result to a RST file. '''

    fname = module_map[module]

    # ignore everything but python source
    if os.path.basename(fname)[-3:] != '.py':
        return

    print "rendering: %s" % module

    doc, examples = get_docstring(fname, verbose=options.verbose)

    # crash if module is missing documentation and not explicitly hidden from
    # docs index

    if doc is None and module not in BLACKLIST_MODULES:
        sys.stderr.write(
            "*** ERROR: CORE MODULE MISSING DOCUMENTATION: %s, %s ***\n"
            % (fname, module)
        )
        sys.exit(1)
    if doc is None:
        return "SKIPPED"
    # ignore any file in BLACKLIST_MODULES
    if module in BLACKLIST_MODULES:
        return "SKIPPED"

    all_keys = []

    if 'version_added' not in doc:
        sys.stderr.write(
            "*** ERROR: missing version_added in: %s ***\n" % module
        )
        sys.exit(1)

    added = 0
    if doc['version_added'] == 'historical':
        del doc['version_added']
    else:
        added = doc['version_added']

    # don't show version added information if it's too old to be called out
    if added:
        added_tokens = str(added).split(".")
        added = added_tokens[0] + "." + added_tokens[1]
        added_float = float(added)
        if added and added_float < TO_OLD_TO_BE_NOTABLE:
            del doc['version_added']

    for (k, v) in doc['parameters'].iteritems():
        all_keys.append(k)
    all_keys = sorted(all_keys)
    doc['option_keys'] = all_keys

    doc['filename'] = fname
    doc['docuri'] = doc['module'].replace('_', '-')
    doc['now_date'] = datetime.date.today().strftime('%Y-%m-%d')
    doc['ansible_version'] = options.ansible_version
    doc['plainexamples'] = examples  # plain text

    # Test for proper values

    for yaml_entry in doc:
        if not doc[yaml_entry]:
            sys.stderr.write(
                "*** ERROR: The %s field can not be left blank.\n"
                % yaml_entry)
            sys.exit(1)

        for param in doc['parameters'].values():
            if type(param['required']) is not bool:
                sys.stderr.write(
                    "*** ERROR: The 'required' field must be True or False.\n")
                sys.exit(1)

            if not param['description']:
                sys.stderr.write(
                    "*** ERROR: 'description' is required for a parameter.\n")
                sys.exit(1)

            if not param['type']:
                sys.stderr.write(
                    "*** ERROR: 'type' is required for a parameter.\n")
                sys.exit(1)

    # here is where we build the table of contents...

    text = template.render(doc)
    write_data(text, options, outputname, module)


def process_category(category, categories, options, env, template, outputname):
    ''' handle directories of modules as a 'category' for documentation '''

    module_map = categories[category]

    category_file_path = os.path.join(options.output_dir,
                                      "list_of_%s_modules.rst" % category)
    category_file = open(category_file_path, "w")
    print "*** recording category %s in %s ***" \
          % (category, category_file_path)

    category = category.replace("_", " ")
    category = category.title()

    modules = module_map.keys()
    modules.sort()

    category_header = "%s Modules" % (category.title())
    underscores = "`" * len(category_header)

    category_file.write("""\
:orphan:

%s
%s

.. toctree::
   :maxdepth: 1


""" % (category_header, underscores))

    for module in modules:
        result = process_module(module,
                                options,
                                env,
                                template,
                                outputname,
                                module_map)

        if result != "SKIPPED":
            if 'pyc' not in module:
                file_name = "   %s_module\n" % module
                category_file.write(file_name.replace(".py", ""))

    category_file.close()


def validate_options(options):
    ''' validate option parser options '''

    if not options.module_dir:
        print >>sys.stderr, "--module-dir is required"
        sys.exit(1)
    if not os.path.exists(options.module_dir):
        print >>sys.stderr, "--module-dir does not exist: %s" \
            % options.module_dir
        sys.exit(1)
    if not options.template_dir:
        print "--template-dir must be specified"
        sys.exit(1)


def main():

    p = generate_parser()

    (options, args) = p.parse_args()
    validate_options(options)

    env, template, outputname = jinja2_environment(options.template_dir,
                                                   options.type)

    categories = list_modules(options.module_dir)
    category_names = categories.keys()
    category_names.sort()

    # Create the 'directives' directory if it doesn't exist
    if not os.path.exists(options.output_dir):
        os.makedirs(options.output_dir)

    category_list_path = os.path.join(options.output_dir,
                                      "modules_by_category.rst")

    category_list_file = open(category_list_path, "w")
    category_list_file.write(":orphan:\n\n")
    category_list_file.write("Module Index\n")
    category_list_file.write("============\n")
    category_list_file.write("\n\n")
    category_list_file.write(".. toctree::\n")
    category_list_file.write("   :maxdepth: 1\n\n")

    category_list_file.write("   list_of_%s_modules\n" % category_names[1])
    process_category(category_names[1],
                     categories,
                     options,
                     env,
                     template,
                     outputname)

    category_list_file.close()


def get_docstring(filename, verbose=False):
    """
    Search for assignment of the DOCUMENTATION and EXAMPLES variables
    in the given file.
    Parse DOCUMENTATION from YAML and return the YAML doc or None
    together with EXAMPLES, as plain text.
    """

    doc = None
    plainexamples = None

    try:
        # Thank you, Habbie, for this bit of code :-)
        M = ast.parse(''.join(open(filename)))
        for child in M.body:
            if isinstance(child, ast.Assign):
                if 'DOCUMENTATION' in (t.id for t in child.targets):
                    doc = yaml.safe_load(child.value.s)

                if 'EXAMPLES' in (t.id for t in child.targets):
                    plainexamples = child.value.s  # Skip first empty line

    except:
        traceback.print_exc()  # temp
        if verbose is True:
            traceback.print_exc()
            print "unable to parse %s" % filename
    return doc, plainexamples

if __name__ == '__main__':
    main()
